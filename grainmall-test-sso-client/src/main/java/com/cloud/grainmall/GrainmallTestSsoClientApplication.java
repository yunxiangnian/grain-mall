package com.cloud.grainmall;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GrainmallTestSsoClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(GrainmallTestSsoClientApplication.class, args);
    }

}
