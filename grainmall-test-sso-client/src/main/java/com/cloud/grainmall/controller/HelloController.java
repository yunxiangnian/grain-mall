package com.cloud.grainmall.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author lisw
 * @create 2021/7/15 22:51
 */
@Controller
public class HelloController {

    /**
     * 无需登录就可访问
     * @return
     */
    @GetMapping("hello")
    @ResponseBody
    public String hello(){
        return "hello";
    }


}
