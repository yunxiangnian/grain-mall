package com.cloud.yxn.search;


import com.cloud.yxn.search.config.ElasticSearchConfig;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.MatchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.Aggregations;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.bucket.terms.TermsAggregationBuilder;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.io.IOException;

@SpringBootTest
class GrainmallSearchApplicationTests {

    @Resource
    RestHighLevelClient client;

    @Test
    void contextLoads() {
        System.out.println(client.getClass());
    }


    @Test
    void searchData() throws IOException {
        SearchRequest searchRequest = new SearchRequest("for_new_three");
        MatchQueryBuilder queryBuilder = QueryBuilders.matchQuery("name", "mill");
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
        sourceBuilder.query(queryBuilder);
        TermsAggregationBuilder ageAgg = AggregationBuilders.terms("ageAgg").field("age").size(10);
        sourceBuilder.aggregation(ageAgg);

        System.out.println("检索条件:" + sourceBuilder.toString());
        searchRequest.source(sourceBuilder);

        SearchResponse response = client.search(searchRequest, ElasticSearchConfig.COMMON_OPTIONS);
        System.out.println(response);
        SearchHits hits = response.getHits();
        SearchHit[] hits1 = hits.getHits();
        for (SearchHit element : hits1){
            /**对结果进行实体类赋值传给前端*/
        }
        /**对聚合操作进行处理*/
        Aggregations aggregations = response.getAggregations();
        /**获取到名称为 ageAgg的聚合查询结果*/
        Terms agg = aggregations.get("ageAgg");
        for (Terms.Bucket bucket : agg.getBuckets()) {
            String age = bucket.getKeyAsString();
            System.out.println("当前的年龄为:" + age + "当前年龄人数有:" + bucket.getDocCount());
        }
        /**有很多种对应的聚合类，可以根据不同的聚合查询来返回对应的聚合类，比如平均值等 Avg*/

    }
}
