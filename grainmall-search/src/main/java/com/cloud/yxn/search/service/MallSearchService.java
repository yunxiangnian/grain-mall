package com.cloud.yxn.search.service;

import com.cloud.yxn.search.vo.SearchParam;
import com.cloud.yxn.search.vo.SearchResult;
import org.springframework.stereotype.Service;

/**
 * @author lisw
 * @create 2021/7/7 10:32
 */
public interface MallSearchService {

    SearchResult search(SearchParam searchParam);

}
