package com.cloud.yxn.search.controller;

import com.cloud.yxn.search.service.MallSearchService;
import com.cloud.yxn.search.vo.SearchParam;
import com.cloud.yxn.search.vo.SearchResult;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * @author lisw
 * @create 2021/7/6 23:17
 */
@Controller
public class SearchController {

    @Resource
    MallSearchService searchService;

    /**
     * 自动将页面传递过来的查询参数封装成对象
     * @param searchParam
     * @return
     */
    @GetMapping("/list.html")
    public String toList(SearchParam searchParam, Model model,HttpServletRequest request){
        String queryString = request.getQueryString();
        searchParam.set_queryString(queryString);
        SearchResult result = searchService.search(searchParam);
        model.addAttribute("result",result);

        return "list";
    }
}
