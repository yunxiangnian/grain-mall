package com.cloud.yxn.search.vo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author lisw
 * @create 2021/7/7 10:34
 */
@Data
public class SearchParam {
    /**全文检索关键字*/
    private String keyword;
    /**商品类型id*/
    private Long catalog3Id;
    /**排序条件
     * sort = saleCount_desc/asc
     * sort = skuPrice_desc/asc ...
     * */
    private String sort;

    /**还会有很多的过滤条件
     * hasStock(是否有货)、skuPrice（价格区间）
     * skuPrice = 1_500/_500/500_(500以下/500以上)
     * */
    private Integer hasStock = 1;
    /**价格区间*/
    private String skuPrice;
    /**品牌id,可以多选*/
    private List<Long> brandId;
    /**属性筛选*/
    private List<String> attrs;
    /**页码*/
    private Integer pageNum;
    /**查询参数*/
    private String _queryString;
}
