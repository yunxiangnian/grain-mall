package com.cloud.yxn.search.constant;

/**
 * @author lisw
 * @create 2021/7/3 11:19
 */

public class ESConstant {
    /**sku数据在es中的索引*/
    public static final String PRODUCT_INDEX = "grainmall_product";
    public static final Integer PAGE_SIZE = 16;
}
