package com.cloud.yxn.search.controller;

import com.cloud.common.exception.BisCodeEnum;
import com.cloud.common.to.es.SkuEsTo;
import com.cloud.common.utils.R;
import com.cloud.yxn.search.service.ProductSaveService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.List;

/**
 * @author lisw
 * @create 2021/7/3 11:07
 * /search/save/product
 */
@RestController()
@RequestMapping("/search/save")
@Slf4j
public class ESSaveController {

    @Resource
    private ProductSaveService productSaveService;

    /**上架商品*/
    @PostMapping("/product")
    public R productStatusUp(@RequestBody List<SkuEsTo> skuEsToList) {
        boolean b = false;
        try {
             b = productSaveService.productStatusUp(skuEsToList);
        } catch (IOException e) {
            log.error("ESSaveController出现商品上架错误: {}",e);
           return R.error(BisCodeEnum.PRODUCT_EXCEPTION.getCode(), BisCodeEnum.PRODUCT_EXCEPTION.getMsg());
        }
        if (b) {
            return R.error(BisCodeEnum.PRODUCT_EXCEPTION.getCode(), BisCodeEnum.PRODUCT_EXCEPTION.getMsg());
        } else {
            return R.ok();
        }
    }

}
