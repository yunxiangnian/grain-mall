package com.cloud.yxn.search.service;

import com.cloud.common.to.es.SkuEsTo;

import java.io.IOException;
import java.util.List;

/**
 * @author lisw
 * @create 2021/7/3 11:12
 */
public interface ProductSaveService {
    boolean productStatusUp(List<SkuEsTo> skuEsToList) throws IOException;

}
