package com.cloud.yxn.search.vo;

import com.cloud.common.to.es.SkuEsTo;
import lombok.Data;

import java.util.List;

/**
 * @author lisw
 * @create 2021/7/7 11:01
 */
@Data
public class SearchResult {
    /**查询到的所有商品信息*/
    private List<SkuEsTo> skuEsTos;
    /**当前页码*/
    private Long pageNum;
    /**总记录数*/
    private Long total;
    /**总页码*/
    private Integer totalPages;
    private List<Integer> pageNavs;
    /**查询到的所有涉及到的所有品牌*/
    private List<BrandVo> brands;
    /**查询到的所有涉及到的品牌的所有属性*/
    private List<AttrVo> attrs;
    /**涉及到的所有分类*/
    private List<CategoryVo> categoryVoList;

    /**面包屑导航*/
    private List<NavsVo> navs;

    @Data
    public static class NavsVo{
        private String navName;
        private String navValue;
        private String link;
    }


    /**============================= 以上是返回给页面的信息*/
    @Data
    public static class BrandVo{
        private Long brandId;
        private String brandName;
        private String brandImg;
    }

    @Data
    public static class CategoryVo{
        private Long categoryId;
        private String categoryName;
    }

    @Data
    public static class AttrVo{
        private Long attrId;
        private String attrName;
        private List<String> attrValue;
    }
}
