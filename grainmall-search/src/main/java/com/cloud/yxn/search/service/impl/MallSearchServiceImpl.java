package com.cloud.yxn.search.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.cloud.common.to.es.SkuEsTo;
import com.cloud.common.utils.R;
import com.cloud.yxn.search.GrainmallSearchApplication;
import com.cloud.yxn.search.config.ElasticSearchConfig;
import com.cloud.yxn.search.constant.ESConstant;
import com.cloud.yxn.search.feign.ProductFeignService;
import com.cloud.yxn.search.service.MallSearchService;
import com.cloud.yxn.search.vo.AttrResponseVo;
import com.cloud.yxn.search.vo.SearchParam;
import com.cloud.yxn.search.vo.SearchResult;
import org.apache.lucene.search.join.ScoreMode;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.NestedQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.RangeQueryBuilder;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.nested.NestedAggregationBuilder;
import org.elasticsearch.search.aggregations.bucket.nested.ParsedNested;
import org.elasticsearch.search.aggregations.bucket.terms.ParsedLongTerms;
import org.elasticsearch.search.aggregations.bucket.terms.ParsedStringTerms;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.bucket.terms.TermsAggregationBuilder;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightField;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author lisw
 * @create 2021/7/7 10:33
 */
@Service("mallSearchService")
public class MallSearchServiceImpl implements MallSearchService {

    @Resource
    private RestHighLevelClient client;

    @Resource
    private ProductFeignService productFeignService;

    @Override
    public SearchResult search(SearchParam searchParam) {
        //1、动态构建查询需要的dsl
        SearchResult searchResult = null;

        SearchRequest searchRequest = buildSearchRequest(searchParam);

        try {
            SearchResponse response = client.search(searchRequest, ElasticSearchConfig.COMMON_OPTIONS);
            searchResult = buildSearchResponse(response,searchParam);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return searchResult;
    }

    private SearchResult buildSearchResponse(SearchResponse response,SearchParam param) {
        //debug 分析response 的内容
        SearchResult result = new SearchResult();
        SearchHits hits = response.getHits();
        List<SkuEsTo> skuEsTos = new ArrayList<>();
        if (null != hits && hits.getHits().length > 0){
            for (SearchHit hit : hits) {
                String string = hit.getSourceAsString();
                SkuEsTo skuEsTo = JSON.parseObject(string, SkuEsTo.class);
                if (!StringUtils.isEmpty(param.getKeyword())){
                    HighlightField skuTitle = hit.getHighlightFields().get("skuTitle");
                    String string1 = skuTitle.getFragments()[0].toString();
                    skuEsTo.setSkuTitle(string1);
                }
                skuEsTos.add(skuEsTo);
            }
        }
        result.setSkuEsTos(skuEsTos);

        /**赋值属性信息*/
        /**分类的属性信息 catalog_agg catalog_name_agg*/
        ParsedLongTerms catalog_agg = response.getAggregations().get("catalog_agg");
        List<? extends Terms.Bucket> buckets = catalog_agg.getBuckets();
        List<SearchResult.CategoryVo> categoryVos = new ArrayList<>();
        for (Terms.Bucket bucket : buckets) {
            SearchResult.CategoryVo categoryVo = new SearchResult.CategoryVo();
            String keyAsString = bucket.getKeyAsString();
            //设置分类id
            categoryVo.setCategoryId(Long.parseLong(keyAsString));

            ParsedStringTerms catalog_name_agg = bucket.getAggregations().get("catalog_name_agg");
            String categoryName = catalog_name_agg.getBuckets().get(0).getKeyAsString();
            categoryVo.setCategoryName(categoryName);
            categoryVos.add(categoryVo);
        }
        result.setCategoryVoList(categoryVos);

        /**brand 设置品牌  brand_agg brand_name_agg brand_img_agg*/
        List<SearchResult.BrandVo> brandVos = new ArrayList<>();
        ParsedLongTerms brand_agg = response.getAggregations().get("brand_agg");
        List<? extends Terms.Bucket> brandBucket = brand_agg.getBuckets();
        for (Terms.Bucket bucket : brandBucket) {
            SearchResult.BrandVo brandVo = new SearchResult.BrandVo();
            long id = bucket.getKeyAsNumber().longValue();
            String brandName = ((ParsedStringTerms) bucket.getAggregations().get("brand_name_agg")).getBuckets().get(0).getKeyAsString();
            String brandImg = ((ParsedStringTerms) bucket.getAggregations().get("brand_img_agg")).getBuckets().get(0).getKeyAsString();
            brandVo.setBrandId(id);
            brandVo.setBrandName(brandName);
            brandVo.setBrandImg(brandImg);
            brandVos.add(brandVo);
        }
        result.setBrands(brandVos);

        /**属性的聚合  attr_agg attr_id_agg attr_name_agg attr_value_agg*/
        List<SearchResult.AttrVo> attrVos = new ArrayList<>();
        ParsedNested attr_agg = response.getAggregations().get("attr_agg");
        ParsedLongTerms attr_id_agg = attr_agg.getAggregations().get("attr_id_agg");
        for (Terms.Bucket bucket : attr_id_agg.getBuckets()){
            SearchResult.AttrVo attrVo = new SearchResult.AttrVo();
            String attrName = ((ParsedStringTerms) bucket.getAggregations().get("attr_name_agg")).getBuckets().get(0).getKeyAsString();

            /**获取属性值，因为有多个*/
            List<String> attrValues = ((ParsedStringTerms) bucket.getAggregations().get("attr_value_agg")).getBuckets().stream().map(k -> {
                return k.getKeyAsString();
            }).collect(Collectors.toList());

            attrVo.setAttrId(bucket.getKeyAsNumber().longValue());
            attrVo.setAttrName(attrName);
            attrVo.setAttrValue(attrValues);
            attrVos.add(attrVo);
        }
        result.setAttrs(attrVos);
        //总记录数 total
        long total = hits.getTotalHits().value;
        result.setTotal(total);
        //总页码
        int totalPages = (int) total % ESConstant.PAGE_SIZE == 0 ? (int) total / ESConstant.PAGE_SIZE : ((int) total / ESConstant.PAGE_SIZE + 1);
        result.setTotalPages(totalPages);

        LinkedList<Integer> page_navs = new LinkedList<Integer>();
        for (int i = 0; i < totalPages; i++) {
            page_navs.add(i + 1);
        }
        result.setPageNavs(page_navs);
        //当前页码
        result.setPageNum(Long.valueOf(param.getPageNum()));

        /**设置面包屑导航 是整体从 属性中去分析对应的值*/
        if (param.getAttrs() != null && param.getAttrs().size() > 0){
            List<SearchResult.NavsVo> navsVos = param.getAttrs().stream().map(k -> {
                SearchResult.NavsVo navsVo = new SearchResult.NavsVo();
                String[] strings = k.split("_");
                R info = productFeignService.info(Long.parseLong(strings[0]));
                if("0".equals(info.get("code"))){
                    AttrResponseVo attr = info.getData("attr", new TypeReference<AttrResponseVo>() {
                    });
                    navsVo.setNavName(attr.getAttrName());
                }else{
                    navsVo.setNavName(strings[0]);
                }
                navsVo.setNavValue(strings[1]);

                /**设置跳转链接*/
                String encode = null;
                try {
                    encode = URLEncoder.encode(k, "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                String replace = param.get_queryString().replace("&attrs=" + encode, "");
                navsVo.setLink("http://search.grainmall.com:81/list.html?" + replace);

                return navsVo;
            }).collect(Collectors.toList());
            result.setNavs(navsVos);
        }


        return result;
    }

    /**
     * 通过参数获取分析出SearchRequest
     * @param searchParam
     * @return
     */
    private SearchRequest buildSearchRequest(SearchParam searchParam) {
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
        //1、构建bool-query
        // 1、1 must 模糊匹配
        BoolQueryBuilder boolQuery = QueryBuilders.boolQuery();
        if (!StringUtils.isEmpty(searchParam.getKeyword())){
            boolQuery.must(QueryBuilders.matchQuery("skuTitle", searchParam.getKeyword()));
        }
        // 1、2 filter 过滤 三级分类id
        if (null != searchParam.getCatalog3Id()){
            boolQuery.filter(QueryBuilders.termQuery("catalogId", searchParam.getCatalog3Id()));
        }
        // 1、2 filter 过滤 三级分类id
        if (null != searchParam.getBrandId() && searchParam.getBrandId().size() > 0){
            boolQuery.filter(QueryBuilders.termsQuery("brandId", searchParam.getBrandId()));
        }

        //1.2 filter 过滤所有的属性
        if (searchParam.getAttrs() != null && searchParam.getAttrs().size() > 0){
            for (String attrStr : searchParam.getAttrs()) {
                BoolQueryBuilder nestedBoolQuery = QueryBuilders.boolQuery();
                String[] s = attrStr.split("_");
                String attrId = s[0];
                String[] attrsValue = s[1].split(":");
                nestedBoolQuery.must(QueryBuilders.termQuery("attrs.attrId", attrId));
                nestedBoolQuery.must(QueryBuilders.termsQuery("attrs.attrValue", attrsValue));
                /**每一个属性都要生成一个nested嵌入查询，*/
                NestedQueryBuilder nestedQuery = QueryBuilders.nestedQuery("attrs", nestedBoolQuery, ScoreMode.None);
                boolQuery.filter(nestedQuery);
            }
        }
        //库存
        boolQuery.filter(QueryBuilders.termQuery("hasStock",searchParam.getHasStock()==1));
        //价格范围
        if (!StringUtils.isEmpty(searchParam.getSkuPrice())){
            RangeQueryBuilder rangeQuery = QueryBuilders.rangeQuery("skuPrice");
            String[] strings = searchParam.getSkuPrice().split("_");
            if (strings.length > 1) {
                rangeQuery.lte(strings[1]).gte(strings[0]);
            } else if (strings.length == 1) {
                if (searchParam.getSkuPrice().startsWith("_")) {
                    rangeQuery.lte(strings[0]);
                }
                if (searchParam.getSkuPrice().endsWith("_")) {
                    rangeQuery.gte(strings[0]);
                }
            }
            boolQuery.filter(rangeQuery);
        }

        sourceBuilder.query(boolQuery);

        /**排序、高亮、分页*/
        if (!StringUtils.isEmpty(searchParam.getSort())){
            String[] split = searchParam.getSort().split("_");
            String sortField = split[0];
            SortOrder order = split[1].equalsIgnoreCase("asc")?SortOrder.ASC:SortOrder.DESC;
            sourceBuilder.sort(sortField, order);
        }
        //分页 from = (pageNum-1) * size
        if (searchParam.getPageNum() ==null){
            searchParam.setPageNum(1);
        }
        sourceBuilder.from(((searchParam.getPageNum()==null?1:searchParam.getPageNum()) - 1) * ESConstant.PAGE_SIZE);
        sourceBuilder.size(ESConstant.PAGE_SIZE);

        //高亮
        if (!StringUtils.isEmpty(searchParam.getKeyword())){
            HighlightBuilder highlightBuilder = new HighlightBuilder();
            highlightBuilder.preTags("<b color='red'>");
            highlightBuilder.postTags("</b>");
            highlightBuilder.field("skuTitle");
            sourceBuilder.highlighter(highlightBuilder);
        }
        /**聚合分析*/
        TermsAggregationBuilder brand_agg = AggregationBuilders.terms("brand_agg");
        brand_agg.field("brandId").size(50);
        /**brand_agg的子聚合*/
        brand_agg.subAggregation(AggregationBuilders.terms("brand_name_agg").field("brandName").size(10));
        brand_agg.subAggregation(AggregationBuilders.terms("brand_img_agg").field("brandImg").size(10));
        sourceBuilder.aggregation(brand_agg);

        /**catalog聚合*/
        TermsAggregationBuilder catalogAgg = AggregationBuilders.terms
                ("catalog_agg").field("catalogId").size(10);
        catalogAgg.subAggregation
                (AggregationBuilders.terms("catalog_name_agg").field("catalogName").size(1));

        sourceBuilder.aggregation(catalogAgg);

        /**属性聚合*/
        NestedAggregationBuilder nested = AggregationBuilders.nested("attr_agg", "attrs");
        TermsAggregationBuilder attr_id_agg = AggregationBuilders.terms("attr_id_agg").field("attrs.attrId");
        attr_id_agg.subAggregation(AggregationBuilders.terms("attr_name_agg").field("attrs.attrName").size(1));
        attr_id_agg.subAggregation(AggregationBuilders.terms("attr_value_agg").field("attrs.attrValue").size(1));
        /**嵌套聚合，因为attrs是属于嵌套式mapping*/
        nested.subAggregation(attr_id_agg);

        sourceBuilder.aggregation(nested);

        String string = sourceBuilder.toString();
        System.out.println("构建的DSL语句是:" + string);

        SearchRequest searchRequest = new SearchRequest(new String[]{ESConstant.PRODUCT_INDEX}, sourceBuilder);
        return searchRequest;
    }
}
