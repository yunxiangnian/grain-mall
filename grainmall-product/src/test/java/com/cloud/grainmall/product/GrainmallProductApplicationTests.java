package com.cloud.grainmall.product;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.yxn.GrainmallProductApplication;
import com.yxn.cloud.product.dao.AttrGroupDao;
import com.yxn.cloud.product.entity.BrandEntity;
import com.yxn.cloud.product.service.BrandService;
import com.yxn.cloud.product.service.CategoryService;
import com.yxn.cloud.product.vo.SpuItemAttrGroupVo;
import lombok.extern.slf4j.Slf4j;
import org.checkerframework.checker.units.qual.K;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.redisson.api.RedissonClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.io.ByteArrayInputStream;
import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = GrainmallProductApplication.class)
@Slf4j
class GrainmallProductApplicationTests {

    @Resource
    BrandService brandService;

//    @Resource
//    OSSClient ossClient;

    @Resource
    RedissonClient redissonClient;

    @Resource
    CategoryService categoryService;

    @Resource
    AttrGroupDao attrGroupDao;

    @Test
    public void testAttrgetById(){
        List<SpuItemAttrGroupVo> attrGroupWithAttrsBySpuId = attrGroupDao.getAttrGroupWithAttrsBySpuId(1L, 225L);
        attrGroupWithAttrsBySpuId.forEach(K -> System.out.println(K));
    }

    @Test
    void contextLoads() {
        BrandEntity entity = new BrandEntity();
//        entity.setName("Iphone");
//        entity.setDescript("电子产品");
//        brandService.save(entity);
        BrandEntity brand = brandService.getOne(new QueryWrapper<BrandEntity>().eq("brand_id", 1L));
        System.out.println(brand);
    }

    @Test
    void createRedission() {
        System.out.println(redissonClient.getClass());
    }

    @Test
    public void testUploadToAliyun(){
        // Endpoint以杭州为例，其它Region请按实际情况填写。
//        String bucketName = "grain-hello";
//        String objectName = "test-String";
//
//        String content = "First OSS Test";
//        ossClient.putObject(bucketName, objectName, new ByteArrayInputStream(content.getBytes()));
//        // 关闭OSSClient。
//        ossClient.shutdown();
//        System.out.println("上传成功");
    }



    @Test
    public void testCategoryPath(){
        Long[] categoryPath = categoryService.findCategoryPath(170L);
        log.info("返回的数据:{}", Arrays.asList(categoryPath));
    }

}
