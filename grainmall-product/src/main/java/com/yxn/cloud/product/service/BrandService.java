package com.yxn.cloud.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yxn.common.utils.PageUtils;
import com.yxn.cloud.product.entity.BrandEntity;

import java.util.Map;

/**
 * 品牌
 *
 * @author lisw
 * @email lisw@gmail.com
 * @date 2020-11-25 15:38:10
 */
public interface BrandService extends IService<BrandEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void updateDetail(BrandEntity brand);
}

