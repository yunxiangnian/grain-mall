package com.yxn.cloud.product.vo;

import lombok.Data;

/**
 * @author lisw
 * @create 2021/6/29 16:08
 */
@Data
public class AttrRespVo extends AttrVo{
    private String catelogName;
    private String groupName;

    private Long[] catelogPath;
}
