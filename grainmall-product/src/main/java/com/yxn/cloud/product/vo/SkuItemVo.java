package com.yxn.cloud.product.vo;

import com.yxn.cloud.product.entity.SkuImagesEntity;
import com.yxn.cloud.product.entity.SkuInfoEntity;
import com.yxn.cloud.product.entity.SpuInfoDescEntity;
import lombok.Data;

import java.util.List;

/**
 * @author lisw
 * @create 2021/7/12 19:07
 */
@Data
public class SkuItemVo {
    /**1\ sku基本信息获取 pms_sku_info */
    SkuInfoEntity info;
    /** 2\ 获取sku的图片信息 pms_sku_images */
    List<SkuImagesEntity> images;

    /**3、获取spu的销售属性组合*/
    List<SkuItemSaleAttrVo> saleAttrs;

    /**4\ 获取spu的介绍*/
    SpuInfoDescEntity desc;

    /**5、获取spu的规格参数信息*/
    List<SpuItemAttrGroupVo> groupAttrs;

}
