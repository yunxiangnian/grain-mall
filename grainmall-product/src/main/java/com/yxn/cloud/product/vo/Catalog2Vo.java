package com.yxn.cloud.product.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author lisw
 * @create 2021/7/3 17:02
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Catalog2Vo {
    /**一级父分类ID*/
    private String catalog1Id;
    /**三级子分类*/
    private List<Catalog3Vo> catalog3List;
    private Long id;
    private String name;

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Catalog3Vo{
        private String catalog2Id;
        private String id;
        private String name;
    }
}
