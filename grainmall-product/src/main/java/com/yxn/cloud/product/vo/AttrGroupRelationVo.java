package com.yxn.cloud.product.vo;

import lombok.Data;

/**
 * @author lisw
 * @create 2021/6/29 17:29
 */
@Data
public class AttrGroupRelationVo {

    private Long attrId;
    private Long attrGroupId;
}
