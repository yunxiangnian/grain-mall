package com.yxn.cloud.product.feign;

import com.cloud.common.to.es.SkuEsTo;
import com.cloud.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * @author lisw
 * @create 2021/7/3 11:41
 */
@FeignClient(value = "grainmall-elasticsearch-service")
public interface SearchFeignService {
    @PostMapping("/search/save/product")
    R productStatusUp(@RequestBody List<SkuEsTo> skuEsToList);
}
