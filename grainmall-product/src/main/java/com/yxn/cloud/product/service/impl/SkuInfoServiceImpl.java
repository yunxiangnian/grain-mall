package com.yxn.cloud.product.service.impl;

import com.yxn.cloud.product.entity.SkuImagesEntity;
import com.yxn.cloud.product.entity.SpuInfoDescEntity;
import com.yxn.cloud.product.service.*;
import com.yxn.cloud.product.vo.SkuItemSaleAttrVo;
import com.yxn.cloud.product.vo.SkuItemVo;
import com.yxn.cloud.product.vo.SpuItemAttrGroupVo;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ThreadPoolExecutor;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yxn.common.utils.PageUtils;
import com.yxn.common.utils.Query;

import com.yxn.cloud.product.dao.SkuInfoDao;
import com.yxn.cloud.product.entity.SkuInfoEntity;

import javax.annotation.Resource;


@Service("skuInfoService")
public class SkuInfoServiceImpl extends ServiceImpl<SkuInfoDao, SkuInfoEntity> implements SkuInfoService {

    @Resource
    private SkuImagesService skuImagesService;

    @Resource
    private SpuInfoDescService spuInfoDescService;

    @Resource
    private AttrGroupService attrGroupService;

    @Resource
    private SkuSaleAttrValueService saleAttrValueService;

    @Resource
    private ThreadPoolExecutor threadPoolExecutor;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<SkuInfoEntity> page = this.page(
                new Query<SkuInfoEntity>().getPage(params),
                new QueryWrapper<SkuInfoEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public void saveSkuInfo(SkuInfoEntity skuInfoEntity) {
        this.baseMapper.insert(skuInfoEntity);
    }


    @Override
    public PageUtils queryPageByCondition(Map<String, Object> params) {
        QueryWrapper<SkuInfoEntity> queryWrapper = new QueryWrapper<>();
        /**
         * key:
         * catelogId: 0
         * brandId: 0
         * min: 0
         * max: 0
         */
        String key = (String) params.get("key");
        if(!StringUtils.isEmpty(key)){
            queryWrapper.and((wrapper)->{
                wrapper.eq("sku_id",key).or().like("sku_name",key);
            });
        }

        String catelogId = (String) params.get("catelogId");
        if(!StringUtils.isEmpty(catelogId)&&!"0".equalsIgnoreCase(catelogId)){

            queryWrapper.eq("catalog_id",catelogId);
        }

        String brandId = (String) params.get("brandId");
        if(!StringUtils.isEmpty(brandId)&&!"0".equalsIgnoreCase(catelogId)){
            queryWrapper.eq("brand_id",brandId);
        }

        String min = (String) params.get("min");
        if(!StringUtils.isEmpty(min)){
            queryWrapper.ge("price",min);
        }

        String max = (String) params.get("max");

        if(!StringUtils.isEmpty(max)  ){
            try{
                BigDecimal bigDecimal = new BigDecimal(max);

                if(bigDecimal.compareTo(new BigDecimal("0"))==1){
                    queryWrapper.le("price",max);
                }
            }catch (Exception e){

            }

        }

        IPage<SkuInfoEntity> page = this.page(
                new Query<SkuInfoEntity>().getPage(params),
                queryWrapper
        );

        return new PageUtils(page);
    }

    @Override
    public List<SkuInfoEntity> querySkusBySpuId(Long spuId) {
       return this.list(new QueryWrapper<SkuInfoEntity>().eq("spu_id", spuId));
    }

    @Override
    public SkuItemVo item(Long skuId) {
        SkuItemVo skuItemVo = new SkuItemVo();
        /**整合异步编排*/
        CompletableFuture<SkuInfoEntity> infoFuture = CompletableFuture.supplyAsync(() -> {
            /**1\ sku基本信息获取 pms_sku_info */
            SkuInfoEntity infoEntity = getById(skuId);
            if (infoEntity != null) {
                skuItemVo.setInfo(infoEntity);
            }
            return infoEntity;
        },threadPoolExecutor);
        CompletableFuture<Void> spuSaleFuture = infoFuture.thenAcceptAsync((res) -> {
            /**3、获取spu的销售属性组合*/
            List<SkuItemSaleAttrVo> skuSaleAttrVo = saleAttrValueService.getSaleAttrsBySpuId(res.getSpuId());
            skuItemVo.setSaleAttrs(skuSaleAttrVo);
        }, threadPoolExecutor);
        CompletableFuture<Void> spuDescFuture = infoFuture.thenAcceptAsync((res) -> {
            /**4\ 获取spu的介绍*/
            SpuInfoDescEntity descEntity = spuInfoDescService.getById(res.getSpuId());
            if (descEntity != null) {
                skuItemVo.setDesc(descEntity);
            }
        }, threadPoolExecutor);
        CompletableFuture<Void> spuAttrFuture = infoFuture.thenAcceptAsync((res) -> {
            /**5、获取spu的规格参数信息*/
            List<SpuItemAttrGroupVo> spuAttrInfos = attrGroupService.getAttrGroupWithAttrsBySpuId(res.getSpuId(), res.getCatalogId());
            skuItemVo.setGroupAttrs(spuAttrInfos);
        }, threadPoolExecutor);

        CompletableFuture<Void> imagesFuture = CompletableFuture.runAsync(() -> {
            /** 2\ 获取sku的图片信息 pms_sku_images */
            List<SkuImagesEntity> images = skuImagesService.getIamgesBySkuId(skuId);
            if (images != null && images.size() > 0) {
                skuItemVo.setImages(images);
            }
        });

        //等待所有任务都完成,阻塞等结果
        CompletableFuture.allOf(spuSaleFuture,spuDescFuture,spuAttrFuture,imagesFuture);


        return skuItemVo;
    }
}
