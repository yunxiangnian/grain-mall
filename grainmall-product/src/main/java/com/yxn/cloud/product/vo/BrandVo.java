package com.yxn.cloud.product.vo;

import lombok.Data;

/**
 * @author lisw
 * @create 2021/6/30 10:44
 */
@Data
public class BrandVo {
    private Long brandId;
    private String brandName;
}
