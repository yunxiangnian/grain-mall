package com.yxn.cloud.product.vo;

import lombok.Data;

/**
 * @author lisw
 * @create 2021/7/12 22:48
 */
@Data
public class SkuItemSaleAttrVo {
    private Long attrId;
    private String attrName;
    private String attrValues;
}
