package com.yxn.cloud.product.dao;

import com.yxn.cloud.product.entity.SpuImagesEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * spu图片
 * 
 * @author lisw
 * @email lisw@gmail.com
 * @date 2020-11-25 15:38:10
 */
@Mapper
public interface SpuImagesDao extends BaseMapper<SpuImagesEntity> {
	
}
