package com.yxn.cloud.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yxn.cloud.product.vo.Catalog2Vo;
import com.yxn.common.utils.PageUtils;
import com.yxn.cloud.product.entity.CategoryEntity;

import java.util.List;
import java.util.Map;

/**
 * 商品三级分类
 *
 * @author lisw
 * @email lisw@gmail.com
 * @date 2020-11-25 15:38:10
 */
public interface CategoryService extends IService<CategoryEntity> {

    PageUtils queryPage(Map<String, Object> params);

    List<CategoryEntity> listWithTree();

    void batchRemoveMenuByIds(List<Long> asList);

    /**
     * 找到CatelogId的完整路径 【父/子/孙】
     * @param catelogId
     * @return
     */
    Long[] findCategoryPath(Long catelogId);

    void updateCascade(CategoryEntity category);

    List<CategoryEntity> selectLevel1Category();

    Map<String, List<Catalog2Vo>> getCatalogJson() throws InterruptedException;

}

