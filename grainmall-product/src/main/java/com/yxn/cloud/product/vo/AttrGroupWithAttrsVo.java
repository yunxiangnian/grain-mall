package com.yxn.cloud.product.vo;

import com.yxn.cloud.product.entity.AttrEntity;
import com.yxn.cloud.product.entity.AttrGroupEntity;
import lombok.Data;

import java.util.List;

/**
 * @author lisw
 * @create 2021/6/30 11:14
 */
@Data
public class AttrGroupWithAttrsVo extends AttrGroupEntity {
    private List<AttrEntity> attrs;
}
