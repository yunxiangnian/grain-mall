package com.yxn.cloud.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yxn.common.utils.PageUtils;
import com.yxn.cloud.product.entity.ProductAttrValueEntity;

import java.util.List;
import java.util.Map;

/**
 * spu属性值
 *
 * @author lisw
 * @email lisw@gmail.com
 * @date 2020-11-25 15:38:10
 */
public interface ProductAttrValueService extends IService<ProductAttrValueEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void saveProductAttr(List<ProductAttrValueEntity> collect);

    List<ProductAttrValueEntity> baseAttrlistforspu(Long spuId);

    void updateSpuAttr(Long spuId, List<ProductAttrValueEntity> entities);

    List<ProductAttrValueEntity> baseAttrListforSpu(Long spuId);

}

