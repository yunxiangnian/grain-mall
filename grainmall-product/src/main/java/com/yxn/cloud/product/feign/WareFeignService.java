package com.yxn.cloud.product.feign;

import com.cloud.common.to.SkuHasStockVo;
import com.cloud.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * @author lisw
 * @create 2021/7/3 10:32
 */
@FeignClient(value = "grainmall-ware-service")
public interface WareFeignService {

    @PostMapping("/ware/waresku/hasstock")
    List<SkuHasStockVo> getSkusHasStock(@RequestBody List<Long> skuIds);
}
