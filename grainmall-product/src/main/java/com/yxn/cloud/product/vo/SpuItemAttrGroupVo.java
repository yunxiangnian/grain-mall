package com.yxn.cloud.product.vo;

import lombok.Data;

import java.util.List;

/**
 * @author lisw
 * @create 2021/7/12 21:49
 */
@Data
public class SpuItemAttrGroupVo {
    private String groupName;
    private List<Attr> baseAttrs;
}
