package com.yxn.cloud.product.feign;

import com.baomidou.mybatisplus.extension.api.R;
import com.cloud.common.to.SkuReductionTo;
import com.cloud.common.to.SpuBoundTo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @author lisw
 * @create 2021/6/30 14:36
 */
@FeignClient(value = "grainmall-coupon-service")
public interface CouponFeignService {

    @PostMapping("/coupon/spubounds/save")
    R saveSpuBounds(@RequestBody SpuBoundTo spuBoundTo);

    @PostMapping("/coupon/skufullreduction/saveInfo")
    R saveSkuReduction(SkuReductionTo skuReductionTo);
}
