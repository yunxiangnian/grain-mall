package com.yxn.cloud.product.service;

import com.baomidou.mybatisplus.extension.api.R;
import com.baomidou.mybatisplus.extension.service.IService;
import com.yxn.cloud.product.vo.AttrGroupRelationVo;
import com.yxn.common.utils.PageUtils;
import com.yxn.cloud.product.entity.AttrAttrgroupRelationEntity;

import java.util.List;
import java.util.Map;

/**
 * 属性&属性分组关联
 *
 * @author lisw
 * @email lisw@gmail.com
 * @date 2020-11-25 15:38:10
 */
public interface AttrAttrgroupRelationService extends IService<AttrAttrgroupRelationEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void saveBatch(List<AttrGroupRelationVo> vos);

}

