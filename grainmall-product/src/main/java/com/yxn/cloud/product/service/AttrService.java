package com.yxn.cloud.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yxn.cloud.product.entity.ProductAttrValueEntity;
import com.yxn.cloud.product.vo.AttrGroupRelationVo;
import com.yxn.cloud.product.vo.AttrRespVo;
import com.yxn.cloud.product.vo.AttrVo;
import com.yxn.common.utils.PageUtils;
import com.yxn.cloud.product.entity.AttrEntity;

import java.util.List;
import java.util.Map;

/**
 * 商品属性
 *
 * @author lisw
 * @email lisw@gmail.com
 * @date 2020-11-25 15:38:10
 */
public interface AttrService extends IService<AttrEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void saveDetailAttr(AttrVo attr);

    PageUtils queryBaseAttrPage(Map<String, Object> params, Long catelogId,String type);

    AttrRespVo getAttrInfo(Long attrId);

    void updateAttr(AttrVo attr);

    List<AttrEntity> getRelationAttr(Long attrgroupId);

    void deleteRelation(AttrGroupRelationVo[] vos);

    PageUtils getNoRelationAttr(Map<String, Object> params, Long attrgroupId);


    List<Long> selectAttrSearchAttrIds(List<Long> attrIds);

}

