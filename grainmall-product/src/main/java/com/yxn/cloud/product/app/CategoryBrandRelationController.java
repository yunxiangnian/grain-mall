package com.yxn.cloud.product.app;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.yxn.cloud.product.entity.BrandEntity;
import com.yxn.cloud.product.vo.BrandVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.yxn.cloud.product.entity.CategoryBrandRelationEntity;
import com.yxn.cloud.product.service.CategoryBrandRelationService;
import com.yxn.common.utils.R;



/**
 * 品牌分类关联
 *
 * @author lisw
 * @email lisw@gmail.com
 * @date 2020-11-25 15:38:10
 */
@RestController
@RequestMapping("product/categorybrandrelation")
public class CategoryBrandRelationController {
    @Autowired
    private CategoryBrandRelationService categoryBrandRelationService;

    /**
     * 查询品牌和分类关联列表
     */
    @GetMapping(value = "/catelog/list")
    public R list(@RequestParam Long brandId){
        List<CategoryBrandRelationEntity> data = categoryBrandRelationService.list(
                new QueryWrapper<CategoryBrandRelationEntity>().eq("brand_id", brandId));

        return R.ok().put("data", data);
    }

    /**
     *
     * @param CatId
     * @return
     * 1、Controller处理请求，接收和校验数据
     * 2、Service接收Controller传来的数据进行业务处理
     * 3、Controller接收Service传来的数据，封装页面指定的Vo
     */
    @GetMapping(value = "/brands/list")
    public R relationBrandList(@RequestParam(value = "catId",required = true) Long CatId){
        List<BrandEntity> brandVos = categoryBrandRelationService.getBrandsByCatId(CatId);

        List<BrandVo> vos = brandVos.stream().map(vo -> {
            BrandVo brandVo = new BrandVo();
            brandVo.setBrandId(vo.getBrandId());
            brandVo.setBrandName(vo.getName());
            return brandVo;
        }).collect(Collectors.toList());
        return R.ok().put("data", vos);
    }

    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    public R info(@PathVariable("id") Long id){
		CategoryBrandRelationEntity categoryBrandRelation = categoryBrandRelationService.getById(id);

        return R.ok().put("categoryBrandRelation", categoryBrandRelation);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody CategoryBrandRelationEntity categoryBrandRelation){
		categoryBrandRelationService.saveDetail(categoryBrandRelation);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody CategoryBrandRelationEntity categoryBrandRelation){
		categoryBrandRelationService.updateById(categoryBrandRelation);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Long[] ids){
		categoryBrandRelationService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
