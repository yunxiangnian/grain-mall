package com.yxn;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;

/**
 * mp 配置逻辑删除
 * 1、在全局配置
 *     - 在yml文件中添加以下配置
 * JSR303校验
 *  1、给Bean加上校验注解即可 具体注解在javax.validation.constraints下 并定义自己的自定义提示
 *  2、在具体形参处加入@Valid注解，此时springmvc才会触发校验。只在Bean中配置是不起作用的。
 *  3、给校验的Bean后紧跟BindingResult，去查看结果信息
 * JSR分组校验
 */
@MapperScan(value = "com.yxn.cloud.product.dao")
@SpringBootApplication
@EnableRedisHttpSession
@EnableDiscoveryClient
@EnableFeignClients(basePackages = {"com.yxn.cloud.product.feign"})
@EnableCaching
public class GrainmallProductApplication {

    public static void main(String[] args) {
        SpringApplication.run(GrainmallProductApplication.class, args);
    }

}
