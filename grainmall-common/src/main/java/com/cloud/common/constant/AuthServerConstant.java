package com.cloud.common.constant;

/**
 * @author lisw
 * @create 2021/7/13 21:58
 */
public class AuthServerConstant {
    public static final String SMS_CODE_REDIS_PREFIX = "sms:code:";
    public static final String LOGIN_USER = "loginUser";
}
