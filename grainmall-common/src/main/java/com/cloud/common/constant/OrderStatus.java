package com.cloud.common.constant;

/**
 * @author lisw
 * @create 2021/8/15 15:29
 */
public enum OrderStatus {
    WAIT_PAY(0, "待付款"),
    WAIT_SEND(1, "代发货"),
    ALREADY_SEND(2, "已发货"),
    FINISHED(3, "已完成"),
    IS_CLOSED(4, "已关闭"),
    NOT_VALID(5, "无效订单");


    private Integer status;
    private String desc;

    private OrderStatus(Integer code, String desc) {
        this.status = code;
        this.desc = desc;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
