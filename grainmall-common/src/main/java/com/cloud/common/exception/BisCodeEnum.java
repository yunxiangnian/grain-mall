package com.cloud.common.exception;

/**
 * @author lisw
 * @create 2021/6/28 17:24
 * 10 : 通用
 * 11： 商品
 * 12： 订单
 * 13： 购物车
 * 14： 物流
 * 15： 用户
 * 21:仓储
 * 异常枚举类
 */
public enum  BisCodeEnum {
    UNKNOWN_EXCEPTION(10001,"未知错误异常"),
    VALID_EXCEPTION(10002,"数据校验异常"),
    VALID_SMS__CODE_EXCEPTION(10003,"短信验证码频率过高"),
    PRODUCT_EXCEPTION(11000,"商品上架异常"),
    USER_EXIST_EXCEPTION(15001,"用户存在异常"),
    PHONE_EXIST_EXCEPTION(15002,"手机号存在异常"),
    NO_STOCK_EXCEPTION(21001,"商品库存不足"),
    LOGINACCT_PASSWORD_INVALID_EXCEPTION(15003,"账户或密码错误");



    private int code;
    private String msg;
    BisCodeEnum(int code,String msg){
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
