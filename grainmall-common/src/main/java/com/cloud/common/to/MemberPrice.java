package com.cloud.common.to;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author lisw
 * @create 2021/6/30 15:10
 * 会员价格
 */
@Data
public class MemberPrice {
    private Long id;
    private String name;
    private BigDecimal price;
}
