package com.cloud.common.to;

import lombok.Data;

/**
 * @author lisw
 * @create 2021/7/3 10:22
 */
@Data
public class SkuHasStockVo {
    private Long skuId;
    private Boolean hasStock;
}
