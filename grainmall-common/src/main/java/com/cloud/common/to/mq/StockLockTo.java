package com.cloud.common.to.mq;

import lombok.Data;

import java.util.List;

/**
 * @author lisw
 * @create 2021/8/12 21:46
 */
@Data
public class StockLockTo {
    /**库存工作单的id*/
    private Long wareTaskId;

    /**库存工作单所有的详情Id*/
    private StockDetailTo stockDetailTo;
}
