package com.cloud.common.to;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author lisw
 * @create 2021/6/30 14:56
 */
@Data
public class SkuReductionTo {
    /**
     * spu_id
     */
    private Long skuId;
    /**
     * 满几件
     */
    private Integer fullCount;
    /**
     * 打几折
     */
    private BigDecimal discount;
    /**
     * 折后价
     */
    private BigDecimal fullPrice;
    private int countStatus;
    private BigDecimal reducePrice;
    private int priceStatus;
    private List<MemberPrice> memberPrice;
}
