package com.cloud.common.to;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author lisw
 * @create 2021/6/30 14:39
 */
@Data
public class SpuBoundTo {
    private Long SpuId;
    private BigDecimal buyBounds;
    private BigDecimal growBounds;

}
