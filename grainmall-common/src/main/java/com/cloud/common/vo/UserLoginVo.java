package com.cloud.common.vo;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * @author lisw
 * @create 2021/7/14 15:13
 */
@Data
@ToString
public class UserLoginVo implements Serializable {
    private String loginacct;
    private String password;
}
