package com.yxn;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;

@SpringBootApplication
@EnableFeignClients(basePackages = "com.yxn.cloud.member.feign")
@EnableDiscoveryClient
@EnableRedisHttpSession
public class GrainmallMemberApplication {

    public static void main(String[] args) {
        SpringApplication.run(GrainmallMemberApplication.class, args);
    }

}
