package com.yxn.cloud.member.vo;

import lombok.Data;

/**
 * @author lisw
 * @create 2021/7/14 15:23
 */
@Data
public class MemberLoginVo {
    private String loginacct;
    private String password;
}
