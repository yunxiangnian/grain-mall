package com.yxn.cloud.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yxn.common.utils.PageUtils;
import com.yxn.cloud.member.entity.MemberLoginLogEntity;

import java.util.Map;

/**
 * 会员登录记录
 *
 * @author lisw
 * @email lisw@gmail.com
 * @date 2020-11-25 17:16:57
 */
public interface MemberLoginLogService extends IService<MemberLoginLogEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

