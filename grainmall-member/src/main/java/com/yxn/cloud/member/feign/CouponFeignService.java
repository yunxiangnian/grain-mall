package com.yxn.cloud.member.feign;

import com.yxn.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author lisw
 * @create 2020/11/25
 */
@FeignClient(value = "grainmall-coupon-service")
public interface CouponFeignService {

    @RequestMapping("/coupon/coupon/member/list")
    R memberCoupons();
}
