package com.yxn.cloud.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yxn.cloud.member.exception.PhoneExistException;
import com.yxn.cloud.member.exception.UserExistException;
import com.yxn.cloud.member.vo.MemberLoginVo;
import com.yxn.cloud.member.vo.MemberUserRegistVo;
import com.yxn.common.utils.PageUtils;
import com.yxn.cloud.member.entity.MemberEntity;

import java.util.Map;

/**
 * 会员
 *
 * @author lisw
 * @email lisw@gmail.com
 * @date 2020-11-25 17:16:57
 */
public interface MemberService extends IService<MemberEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void regist(MemberUserRegistVo vo);

    void checkPhoneUnique(String phone) throws PhoneExistException;

    void checkUsernameUnique(String username) throws UserExistException;

    MemberEntity login(MemberLoginVo vo);

}

