package com.yxn.cloud.member.controller;

import java.util.Arrays;
import java.util.Map;

import com.cloud.common.exception.BisCodeEnum;
import com.cloud.common.utils.R;
import com.yxn.cloud.member.exception.PhoneExistException;
import com.yxn.cloud.member.exception.UserExistException;
import com.yxn.cloud.member.feign.CouponFeignService;
import com.yxn.cloud.member.vo.MemberLoginVo;
import com.yxn.cloud.member.vo.MemberUserRegistVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import com.yxn.cloud.member.entity.MemberEntity;
import com.yxn.cloud.member.service.MemberService;
import com.yxn.common.utils.PageUtils;

import javax.annotation.Resource;


/**
 * 会员
 *
 * @author lisw
 * @email lisw@gmail.com
 * @date 2020-11-25 17:16:57
 */
@RestController
@RequestMapping("member/member")
public class MemberController {
    @Autowired
    private MemberService memberService;

    @Resource
    private CouponFeignService couponFeignService;


    @PostMapping("/login")
    public R login(@RequestBody MemberLoginVo vo){
        MemberEntity memberEntity = memberService.login(vo);
        if (null != memberEntity){
            return new R().setData(memberEntity);
        }else{
            return R.error(BisCodeEnum.LOGINACCT_PASSWORD_INVALID_EXCEPTION.getCode()
                    , BisCodeEnum.LOGINACCT_PASSWORD_INVALID_EXCEPTION.getMsg());
        }
    }

    @PostMapping("/regist")
    public R regist(@RequestBody MemberUserRegistVo vo){
        try {
            memberService.regist(vo);
        } catch (PhoneExistException e) {
            return R.error(BisCodeEnum.PHONE_EXIST_EXCEPTION.getCode(), BisCodeEnum.PHONE_EXIST_EXCEPTION.getMsg());
        } catch (UserExistException e) {
            return R.error(BisCodeEnum.USER_EXIST_EXCEPTION.getCode(), BisCodeEnum.USER_EXIST_EXCEPTION.getMsg());
        }
        return R.ok();
    }

    @RequestMapping(value = "/coupons")
    public R getAllCoupons(){
        MemberEntity memberEntity = new MemberEntity();
        memberEntity.setNickname("张三");
        com.yxn.common.utils.R coupons = couponFeignService.memberCoupons();
        return R.ok().put("member",memberEntity).put("membersCoupons", coupons.get("coupons"));
    }

    /**
     * 列表
     */
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = memberService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    public R info(@PathVariable("id") Long id){
		MemberEntity member = memberService.getById(id);

        return R.ok().put("member", member);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody MemberEntity member){
		memberService.save(member);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody MemberEntity member){
		memberService.updateById(member);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Long[] ids){
		memberService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
