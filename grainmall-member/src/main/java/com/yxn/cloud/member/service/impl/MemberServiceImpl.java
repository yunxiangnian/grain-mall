package com.yxn.cloud.member.service.impl;

import com.yxn.cloud.member.dao.MemberLevelDao;
import com.yxn.cloud.member.entity.MemberLevelEntity;
import com.yxn.cloud.member.exception.PhoneExistException;
import com.yxn.cloud.member.exception.UserExistException;
import com.yxn.cloud.member.vo.MemberLoginVo;
import com.yxn.cloud.member.vo.MemberUserRegistVo;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yxn.common.utils.PageUtils;
import com.yxn.common.utils.Query;

import com.yxn.cloud.member.dao.MemberDao;
import com.yxn.cloud.member.entity.MemberEntity;
import com.yxn.cloud.member.service.MemberService;
import org.springframework.util.DigestUtils;

import javax.annotation.Resource;


@Service("memberService")
public class MemberServiceImpl extends ServiceImpl<MemberDao, MemberEntity> implements MemberService {

    @Resource
    MemberLevelDao memberLevelDao;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<MemberEntity> page = this.page(
                new Query<MemberEntity>().getPage(params),
                new QueryWrapper<MemberEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public void regist(MemberUserRegistVo vo) {
        MemberEntity memberEntity = new MemberEntity();
        //设置默认等级
        MemberLevelEntity entity = memberLevelDao.getDefaultLevel();
        memberEntity.setLevelId(entity.getId());
        //此时采用了异常机制来处理判断机制
        checkPhoneUnique(vo.getPhone());
        checkUsernameUnique(vo.getUserName());

        memberEntity.setMobile(vo.getPhone());
        memberEntity.setUsername(vo.getUserName());

        //设置密码。密码的加密存储
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        //盐值加密的密文
        String encode = encoder.encode(vo.getPassword());
        memberEntity.setPassword(encode);

        //其他的默认信息


        baseMapper.insert(memberEntity);
    }

    @Override
    public void checkPhoneUnique(String phone) throws PhoneExistException{

        Integer integer = baseMapper.selectCount(new QueryWrapper<MemberEntity>().eq("mobile", phone));
        if (null != integer && integer > 0){
            throw new PhoneExistException();
        }
    }

    @Override
    public void checkUsernameUnique(String username) throws UserExistException{
        Integer integer = baseMapper.selectCount(new QueryWrapper<MemberEntity>().eq("username", username));
        if (null != integer && integer > 0){
            throw new UserExistException();
        }

    }

    @Override
    public MemberEntity login(MemberLoginVo vo) {
        String loginacct = vo.getLoginacct();
        String password = vo.getPassword();

        MemberEntity memberEntity = baseMapper.selectOne(new QueryWrapper<MemberEntity>().eq("username", loginacct)
                .or().eq("mobile", loginacct));
        if (memberEntity == null){
            //查无此人
            return null;
        }
        String passwordDb = memberEntity.getPassword();

        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        boolean matches = encoder.matches(password, passwordDb);
        if (matches){
            return memberEntity;
        } else {
            return null;
        }
    }

}
