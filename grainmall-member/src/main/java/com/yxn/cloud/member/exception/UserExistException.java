package com.yxn.cloud.member.exception;

/**
 * @author lisw
 * @create 2021/7/14 11:06
 */
public class UserExistException extends RuntimeException{

    public UserExistException(){
        super("用户名已存在");
    }
}
