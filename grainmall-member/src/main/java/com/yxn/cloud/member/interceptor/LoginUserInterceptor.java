package com.yxn.cloud.member.interceptor;

import com.cloud.common.constant.AuthServerConstant;
import com.cloud.common.vo.MemberRespVo;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author lisw
 * @create 2021/7/26 15:31
 */
@Component
public class LoginUserInterceptor implements HandlerInterceptor {

    public static ThreadLocal<MemberRespVo> threadLocal = new ThreadLocal<>();

    @Override
    public boolean preHandle(HttpServletRequest request,
                             HttpServletResponse response, Object handler) throws Exception {
        String uri = request.getRequestURI();
        //解决远程调用order服务查询order订单状态访问到登录页的问题。
        boolean match = new AntPathMatcher().match("/member/**", uri);
        if (match) {
            return true;
        }

        MemberRespVo loginUser = (MemberRespVo) request.getSession().getAttribute(AuthServerConstant.LOGIN_USER);
        if (null != loginUser){
            threadLocal.set(loginUser);
            return true;
        }else{
            //没有登录
            response.sendRedirect("http://auth.grainmall.com:81/login");
            return false;
        }
    }
}
