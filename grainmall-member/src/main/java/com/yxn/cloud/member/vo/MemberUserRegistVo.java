package com.yxn.cloud.member.vo;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

/**
 * @author lisw
 * @create 2021/7/13 22:21
 */
@Data
public class MemberUserRegistVo {

    private String userName;

    private String password;

    private String phone;

    private String code;
}
