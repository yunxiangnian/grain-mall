package com.yxn.cloud.member.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author lisw
 * @create 2021/8/22 15:37
 */
@Controller
public class MemberWebController {

    @GetMapping("/memberOrder.html")
    public String memberOrderPage(){
        // 查出当前登录用户的所有的订单列表数据


        return "orderList";
    }
}
