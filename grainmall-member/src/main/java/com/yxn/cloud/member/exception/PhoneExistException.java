package com.yxn.cloud.member.exception;

/**
 * @author lisw
 * @create 2021/7/14 11:05
 */
public class PhoneExistException extends RuntimeException{
    public PhoneExistException(){
        super("手机号已存在");
    }
}
