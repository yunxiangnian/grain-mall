package com.yxn.cloud.member.dao;

import com.yxn.cloud.member.entity.MemberEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员
 * 
 * @author lisw
 * @email lisw@gmail.com
 * @date 2020-11-25 17:16:57
 */
@Mapper
public interface MemberDao extends BaseMapper<MemberEntity> {
	
}
