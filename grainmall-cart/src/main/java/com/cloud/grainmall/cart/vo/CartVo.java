package com.cloud.grainmall.cart.vo;

import java.math.BigDecimal;
import java.util.List;

/**
 * 购物车架构
 * @author lisw
 * @create 2021/7/16 22:04
 */
public class CartVo {
    /**商品的总数量*/
    private Integer countNum;
    private List<CartItem> items;
    /**商品类型的数量*/
    private Integer countType;
    /**商品总价*/
    private BigDecimal totalAmount;
    /**减免价格*/
    private BigDecimal reduce = new BigDecimal("0.00");

    public Integer getCountNum() {
        int count = 0;
        if (this.items != null && items.size() > 0){
            for (CartItem item : items) {
                count += item.getCount();
            }
        }
        return count;
    }


    public List<CartItem> getItems() {
        return items;
    }

    public void setItems(List<CartItem> items) {
        this.items = items;
    }

    public Integer getCountType() {
        int count = 0;
        if (this.items != null && items.size() > 0){
            count = items.size();
        }
        return count;
    }


    public BigDecimal getTotalAmount() {
        BigDecimal bigDecimal = new BigDecimal("0");
        if (this.items != null && items.size() > 0){
            for (CartItem item : items) {
                if (item.getCheck()){
                    BigDecimal totalPrice = item.getTotalPrice();
                    bigDecimal  = bigDecimal.add(totalPrice);
                }
            }
        }
        bigDecimal = bigDecimal.subtract(getReduce());
        return bigDecimal;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public BigDecimal getReduce() {
        return reduce;
    }

    public void setReduce(BigDecimal reduce) {
        this.reduce = reduce;
    }
}
