package com.cloud.grainmall.cart.constant;

/**
 * @author lisw
 * @create 2021/7/17 10:03
 */
public class CartConstant {
    public static final String TEMP_USER_COOKIE_NAME = "user-key";
    public static final int TEMP_USER_COOKIE_TIME = 60*60*24*30;
}
