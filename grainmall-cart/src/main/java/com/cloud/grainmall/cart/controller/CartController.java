package com.cloud.grainmall.cart.controller;

import com.cloud.grainmall.cart.interceptor.CartInterceptor;
import com.cloud.grainmall.cart.service.CartService;
import com.cloud.grainmall.cart.to.UserInfoTo;
import com.cloud.grainmall.cart.vo.CartItem;
import com.cloud.grainmall.cart.vo.CartVo;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author lisw
 * @create 2021/7/17 9:42
 */
@Controller
public class CartController {

    @Resource
    CartService cartService;


    @GetMapping("/currentUserCartItems")
    @ResponseBody
    public List<CartItem> getCurrentUserItems(){
        return cartService.getUserCartItems();
    }


    @GetMapping("/checkItem")
    public String checkItem(@RequestParam("skuId")Long skuId,
                            @RequestParam("check")Integer check){
        cartService.checkItem(skuId,check);

        return "redirect:http://cart.grainmall.com:81/cart.html";
    }


    /**
     * 浏览器中有一个cookie，user-key用来表示用户的身份，过期时间为一个月
     * 第一次：没有临时用户，还需创建一个临时用户
     * 登录：在session中
     * 未登录：按照cookie里的user-key获取
     * @return
     */
    @GetMapping("/cart.html")
    public String cartListPage(Model model){
        //1、快速得到用户信息 使用ThreadLocal
        CartVo vo = cartService.getCart(model);
        model.addAttribute("cart", vo);
        return "cartList";
    }

    /**
     * attributes.addFlashAttribute() 将数据放到session中，可以在页面取出，但是只能取一次
     * @param skuId
     * @param number
     * @param attributes
     * @return
     */
    @GetMapping("/addToCart")
    public String addToCart(@RequestParam("skuId")Long skuId, @RequestParam("num")Integer number,
                            RedirectAttributes attributes){
        cartService.addToCart(skuId,number);
        //重定向RedirectAttributes中的数据，会自动在地址栏中拼接上去
        attributes.addAttribute("skuId", skuId);

        return "redirect:http://cart.grainmall.com:81/addToCartSuccess.html";
    }

    /**
     * 跳转到成功页，防止重复提交问题
     * @param skuId
     * @param model
     * @return
     */
    @GetMapping("/addToCartSuccess.html")
    public String addToCartSuccess(@RequestParam("skuId")Long skuId,Model model){
        //重定向页面之后，防止重复点击链接，提交添加url。也就是防止重复提交的问题
        //因为重定向的数据传输问题，所以直接再次查询对应的cartItem即可
        CartItem cartItem = cartService.getCartItem(skuId);
        model.addAttribute("cartItem", cartItem);
        return "success";
    }

}
