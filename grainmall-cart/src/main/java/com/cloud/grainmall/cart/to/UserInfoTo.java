package com.cloud.grainmall.cart.to;

import lombok.Data;

/**
 * @author lisw
 * @create 2021/7/17 9:59
 */
@Data
public class UserInfoTo {
    private Long userId;
    private String userKey;

    private boolean isTempKey;
}
