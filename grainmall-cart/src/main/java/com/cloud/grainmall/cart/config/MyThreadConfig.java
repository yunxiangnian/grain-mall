package com.cloud.grainmall.cart.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @author lisw
 * @create 2021/7/12 23:27
 */
@Configuration
public class MyThreadConfig {

    @Bean
    public ThreadPoolExecutor threadPoolExecutor(ThreadPoolProperties threadPoolProperties){
        return new org.apache.tomcat.util.threads.ThreadPoolExecutor
                (threadPoolProperties.getCore(), threadPoolProperties.getMaxSize(),
                        threadPoolProperties.getTimeKeepAlive(), TimeUnit.SECONDS,
                        new LinkedBlockingQueue<>(100000),
                        Executors.defaultThreadFactory(), new ThreadPoolExecutor.AbortPolicy());
    }
}
