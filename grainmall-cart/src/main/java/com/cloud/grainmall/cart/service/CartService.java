package com.cloud.grainmall.cart.service;

import com.cloud.grainmall.cart.vo.CartItem;
import com.cloud.grainmall.cart.vo.CartVo;
import org.springframework.ui.Model;

import java.util.List;

/**
 * @author lisw
 * @create 2021/7/16 22:22
 */
public interface CartService {
    CartItem addToCart(Long skuId, Integer number);

    CartItem getCartItem(Long skuId);

    CartVo getCart(Model model);

    void clearCart(String cartKey);

    void checkItem(Long skuId, Integer check);

    List<CartItem> getUserCartItems();
}
