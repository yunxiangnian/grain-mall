package com.cloud.grainmall.cart.feign;

import com.cloud.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author lisw
 * @create 2021/7/17 13:00
 */
@FeignClient("grainmall-product-service")
public interface ProductFeignService {
    @RequestMapping("/product/skuinfo/info/{skuId}")
    R getSkuInfo(@PathVariable("skuId") Long skuId);


    @GetMapping("/product/skusaleattrvalue/stringlist/{skuId}")
    List<String> getSkuSaleAttrList(@PathVariable("skuId")Long skuId);

    @GetMapping("/product/skuinfo/{skuId}/price")
    R queryCurrentPrice(@PathVariable("skuId")Long skuId);
}
