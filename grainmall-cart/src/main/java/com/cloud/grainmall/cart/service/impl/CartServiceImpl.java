package com.cloud.grainmall.cart.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.cloud.common.utils.R;
import com.cloud.grainmall.cart.feign.ProductFeignService;
import com.cloud.grainmall.cart.interceptor.CartInterceptor;
import com.cloud.grainmall.cart.service.CartService;
import com.cloud.grainmall.cart.to.UserInfoTo;
import com.cloud.grainmall.cart.vo.CartItem;
import com.cloud.grainmall.cart.vo.CartVo;
import com.cloud.grainmall.cart.vo.SkuInfoVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.BoundHashOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.stream.Collectors;

/**
 * @author lisw
 * @create 2021/7/16 22:22
 */
@Service("cartService")
@Slf4j
public class CartServiceImpl implements CartService {

    @Resource
    StringRedisTemplate stringRedisTemplate;

    @Resource
    ProductFeignService productFeignService;

    @Resource
    ThreadPoolExecutor executor;

    public static final String CART_PREFIX = "grainmall:cart:";

    @Override
    public CartItem addToCart(Long skuId, Integer number) {
        //1、快速得到用户信息 使用ThreadLocal
        BoundHashOperations<String, Object, Object> hashOperations = getCartOps();
        //判断该用户的购物车中是否存在该商品，如果存在，则更新count即可
        if (hashOperations.get(skuId.toString()) != null){
            //如果存在数据，就直接更新count
            CartItem item = JSON.parseObject(String.valueOf(hashOperations.get(skuId.toString())), CartItem.class);
            Integer count = item.getCount() + number;
            item.setCount(count);
            String itemJson = JSON.toJSONString(item);
            hashOperations.put(skuId.toString(), itemJson);
            return item;
        }else{
            //添加到购物车
            CartItem cartItem = new CartItem();
            CompletableFuture<Void> getSkuInfoTask = CompletableFuture.runAsync(() -> {
                R skuInfo = productFeignService.getSkuInfo(skuId);
                SkuInfoVo vo = skuInfo.getData("skuInfo",new TypeReference<SkuInfoVo>(){});
                cartItem.setSkuId(skuId);
                cartItem.setImage(vo.getSkuDefaultImg());
                cartItem.setPrice(vo.getPrice());
                cartItem.setTitle(vo.getSkuTitle());
                cartItem.setCount(number);
            }, executor);
            //查出sku的组合信息 采用异步的方式
            CompletableFuture<Void> getSaleAttrTask = CompletableFuture.runAsync(() -> {
                List<String> strings = productFeignService.getSkuSaleAttrList(skuId);
                cartItem.setSkuAttr(strings);
            },executor);
            try {
                //需要使用allof的get方法才能
                CompletableFuture.allOf(getSkuInfoTask,getSaleAttrTask).get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
            //如果不存在，就插入
            String cartItemJson = JSON.toJSONString(cartItem);
            hashOperations.put(skuId.toString(), cartItemJson);
            return cartItem;
        }
    }

    @Override
    public CartItem getCartItem(Long skuId) {
        BoundHashOperations<String, Object, Object> cartOps = getCartOps();
        CartItem cartItem = JSON.parseObject(cartOps.get(String.valueOf(skuId)).toString(), CartItem.class);
        return cartItem;
    }

    @Override
    public CartVo getCart(Model model) {
        CartVo cartVo = new CartVo();
        UserInfoTo userInfoTo = CartInterceptor.threadLocal.get();
        if (userInfoTo.getUserId() != null){
            //1、已经登录 还要合并没有登录情况下加入购物车中的数据
            List<CartItem> tempCartItemList = getCartItems(CART_PREFIX + userInfoTo.getUserKey());
            if (tempCartItemList != null){
                for (CartItem cartItem : tempCartItemList) {
                    addToCart(cartItem.getSkuId(), cartItem.getCount());
                }
                //清除临时购物车的数据
                clearCart(CART_PREFIX + userInfoTo.getUserKey());
            }
            //3、再来获取登录合并后的数据
            List<CartItem> loginItems = getCartItems(CART_PREFIX + userInfoTo.getUserId());
            cartVo.setItems(loginItems);

            return cartVo;
        }else{
            //2、没有登录
            String cartKey = CART_PREFIX + userInfoTo.getUserKey();
            List<CartItem> cartItems = getCartItems(cartKey);
            cartVo.setItems(cartItems);
            return cartVo;
        }
    }

    private BoundHashOperations<String, Object, Object> getCartOps() {
        UserInfoTo userInfoTo = CartInterceptor.threadLocal.get();
        String cartKey = "";
        if (userInfoTo.getUserId() != null){
            cartKey = CART_PREFIX + userInfoTo.getUserId();
        }else{
            cartKey = CART_PREFIX + userInfoTo.getUserKey();
        }
        //绑定要操作的hashkey，所有的操作都是对这个key进行的
        return stringRedisTemplate.boundHashOps(cartKey);
    }


    private List<CartItem> getCartItems(String cartKey){
        List<Object> values = stringRedisTemplate.opsForHash().values(cartKey);
        if (values!=null && values.size()>0){
            List<CartItem> collect = values.stream().map((obj) -> {
                String str = (String) obj;
                CartItem cartItem = JSON.parseObject(str, CartItem.class);
                return cartItem;
            }).collect(Collectors.toList());
            return collect;
        }
        return null;
    }

    @Override
    public void clearCart(String cartKey){
        stringRedisTemplate.delete(cartKey);
    }

    /**
     * 勾选购物项
     * @param skuId
     * @param check
     */
    @Override
    public void checkItem(Long skuId, Integer check) {
        BoundHashOperations<String, Object, Object> cartOps = getCartOps();
        CartItem cartItem = getCartItem(skuId);
        cartItem.setCheck(check==1?true:false);
        String s = JSON.toJSONString(cartItem);
        cartOps.put(skuId.toString(), s);
    }

    @Override
    public List<CartItem> getUserCartItems() {
        UserInfoTo userInfoTo = CartInterceptor.threadLocal.get();
        if (userInfoTo == null){
            return null;
        }else{
            String cartKey = CART_PREFIX + userInfoTo.getUserId();
            List<CartItem> cartItems = getCartItems(cartKey);
            if (cartItems != null){
                List<CartItem> currentCheckCartItem = cartItems.stream().filter(cartItem -> {
                    return cartItem.getCheck();
                }).map(cartItem -> {
                    //更新为最新价格
                    R currentPrice = productFeignService.queryCurrentPrice(cartItem.getSkuId());
                    String price = (String) currentPrice.get("data");
                    cartItem.setPrice(new BigDecimal(price));
                    return cartItem;
                })  .collect(Collectors.toList());
                return currentCheckCartItem;
            }else {
                return null;
            }
        }
    }
}
