package com.cloud.grainmall.cart.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author lisw
 * @create 2021/7/12 23:34
 */
@ConfigurationProperties(prefix = "grainmall.thread")
@Component
@Data
public class ThreadPoolProperties {
    private Integer core;
    private Integer maxSize;
    private Integer timeKeepAlive;
}
