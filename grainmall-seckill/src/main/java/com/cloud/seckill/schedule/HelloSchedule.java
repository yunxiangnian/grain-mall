package com.cloud.seckill.schedule;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * @author lisw
 * @create 2021/8/22 16:51
 * @EnableScheduling 开启定时任务
 * @Scheduled(cron = "* * * * * ?") 开启一个定时任务
 * @EnableAsync 开启异步任务
 *
 */
@Slf4j
@Component
public class HelloSchedule {

    /**
     * 1、spring中只允许6位，不允许第7位 年
     * 2、spring中的周一-周日 就是1-7 MON-SUN
     * 2、quartz中的周日-周六 是 1-7 SUN-SAT
     * 3、定时任务不应该阻塞
     *      1）、可以使用异步的方式运行
     *      2）、支持定时任务线程池
     *             spring.task.scheduling.pool.size=5
     *      3）、使用异步任务（spring是支持的）
     *
     */
//    @Async
//    @Scheduled(cron = "* * * * * ?")
    public void hello(){
        log.info("schedule ===============> hello");
    }

}
