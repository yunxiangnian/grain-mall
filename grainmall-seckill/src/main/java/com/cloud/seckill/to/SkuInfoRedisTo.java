package com.cloud.seckill.to;

import com.cloud.seckill.vo.SeckillSkuVo;
import com.cloud.seckill.vo.SkuInfoVo;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author lisw
 * @create 2021/8/23 21:27
 */
@Data
public class SkuInfoRedisTo {
    private Long id;
    /**
     * 活动id
     */
    private Long promotionId;
    /**
     * 活动场次id
     */
    private Long promotionSessionId;
    /**
     * 商品id
     */
    private Long skuId;

    /**商品秒杀随机码*/
    private String randomCode;

    /**
     * 秒杀价格
     */
    private BigDecimal seckillPrice;
    /**
     * 秒杀总量
     */
    private BigDecimal seckillCount;
    /**
     * 每人限购数量
     */
    private BigDecimal seckillLimit;
    /**
     * 排序
     */
    private Integer seckillSort;

    private SkuInfoVo skuInfo;

    /**当前秒杀的开始时间*/
    private Long startTime;

    /**当前秒杀的结束时间*/
    private Long endTime;

}
