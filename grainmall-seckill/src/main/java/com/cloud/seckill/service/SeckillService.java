package com.cloud.seckill.service;

import com.cloud.seckill.to.SkuInfoRedisTo;

import java.util.List;

/**
 * @author lisw
 * @create 2021/8/22 17:25
 */
public interface SeckillService {
    void uploadSeckillSkuLatest3Days();

    List<SkuInfoRedisTo> queryCurrentSeckill();

}
