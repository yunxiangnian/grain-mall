package com.cloud.seckill.controller;

import com.cloud.common.utils.R;
import com.cloud.seckill.service.SeckillService;
import com.cloud.seckill.to.SkuInfoRedisTo;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author lisw
 * @create 2021/8/29 16:46
 */
@RestController
public class SeckillController {

    @Resource
    private SeckillService seckillService;

    /**
     * 获取当前秒杀的商品列表
     * @return
     */
    @GetMapping("/currentSeckillSkus")
    public R queryCurrentSeckillSkus(){
        List<SkuInfoRedisTo> skuRedisTo = seckillService.queryCurrentSeckill();
        return R.ok().setData(skuRedisTo);
    }
}
