package com.cloud.seckill.config;

import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;

/**
 * @author lisw
 * @create 2021/7/5 22:01
 */
@Configuration
public class MyRedissonConfig {
    /**
     * 所有对 redisson 的使用都是通过 redissonClient对象
     * @return
     * @throws IOException
     */
    @Bean(destroyMethod="shutdown")
    RedissonClient redissonClient() throws IOException {
        // 创建实例
        Config config = new Config();
        config.useSingleServer().setAddress("redis://192.168.56.10:6379");
        //根据config 创建实例
        return Redisson.create(config);
    }
}
