package com.cloud.seckill.config;

import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @author lisw
 * @create 2021/8/22 17:18
 */
@EnableAsync
@EnableScheduling
public class ScheduleConfig {

}
