package com.cloud.grainmall;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GrainmallTestSsoServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(GrainmallTestSsoServerApplication.class, args);
    }

}
