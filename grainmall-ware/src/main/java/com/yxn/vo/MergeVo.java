package com.yxn.vo;

import lombok.Data;

import java.util.List;

/**
 * @author lisw
 * @create 2021/6/30 17:02
 */
@Data
public class MergeVo {
    private Long purchaseId;
    private List<Long>  items;
}
