package com.yxn.vo;

import lombok.Data;

/**
 * @author lisw
 * @create 2021/8/2 20:31
 */
@Data
public class OrderResultVo {
    /**skuId*/
    private Long skuId;
    /**锁定的数量*/
    private Integer number;
    /**是否成功锁定*/
    private Boolean locked;
}
