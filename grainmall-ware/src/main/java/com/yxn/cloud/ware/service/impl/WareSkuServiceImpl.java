package com.yxn.cloud.ware.service.impl;

import com.cloud.common.to.mq.StockDetailTo;
import com.cloud.common.to.mq.StockLockTo;
import com.yxn.cloud.ware.entity.WareOrderTaskDetailEntity;
import com.yxn.cloud.ware.entity.WareOrderTaskEntity;
import com.yxn.cloud.ware.exception.NoStockException;
import com.yxn.cloud.ware.service.WareOrderTaskDetailService;
import com.yxn.cloud.ware.service.WareOrderTaskService;
import com.yxn.vo.OrderItemVo;
import com.yxn.vo.SkuHasStockVo;
import com.yxn.vo.WareSkuLockVo;
import lombok.Data;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yxn.common.utils.PageUtils;
import com.yxn.common.utils.Query;

import com.yxn.cloud.ware.dao.WareSkuDao;
import com.yxn.cloud.ware.entity.WareSkuEntity;
import com.yxn.cloud.ware.service.WareSkuService;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


@Service("wareSkuService")
@RabbitListener(queues = "stock.release.stock.queue")
public class WareSkuServiceImpl extends ServiceImpl<WareSkuDao, WareSkuEntity> implements WareSkuService {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Resource
    RabbitTemplate rabbitTemplate;

    @Resource
    WareOrderTaskDetailService wareOrderTaskDetailService;

    @Resource
    WareOrderTaskService wareOrderTaskService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        QueryWrapper<WareSkuEntity> queryWrapper = new QueryWrapper<WareSkuEntity>();
        String sku_id = (String) params.get("skuId");
        if (StringUtils.isNotEmpty(sku_id)) {
            queryWrapper.eq("sku_id", sku_id);
        }
        String wareId = (String) params.get("wareId");
        if (StringUtils.isNotEmpty(wareId)) {
            queryWrapper.eq("ware_id", wareId);
        }

        IPage<WareSkuEntity> page = this.page(
                new Query<WareSkuEntity>().getPage(params),
                queryWrapper
        );

        return new PageUtils(page);
    }

    @Override
    public List<SkuHasStockVo> getSkuHasStock(List<Long> skuIds) {
        List<SkuHasStockVo> vos = skuIds.stream().map(item -> {
            SkuHasStockVo stockVo = new SkuHasStockVo();
            Long count = baseMapper.getSkuHasStock(item);
            stockVo.setSkuId(item);
            stockVo.setHasStock(count == null ? false : count > 0);
            return stockVo;
        }).collect(Collectors.toList());
        return vos;
    }

    /**
     * 为某个订单锁定库存
     *
     * @param vo
     * @return
     */
    @Transactional(rollbackFor = NoStockException.class)
    @Override
    public Boolean orderLockStock(WareSkuLockVo vo) {
        /**
         * 保存工作单详情
         * 用来追溯订单
         */
        WareOrderTaskEntity wareOrderTaskEntity = new WareOrderTaskEntity();
        wareOrderTaskEntity.setOrderSn(vo.getOrderSn());
        wareOrderTaskService.save(wareOrderTaskEntity);


        /**1、 找到商品在哪个仓库中有库存，会按照收货地址就近发货*/
        List<OrderItemVo> items = vo.getItems();
        List<SkuStockWare> skuStockWareList = items.stream().map(item -> {
            SkuStockWare skuStockWare = new SkuStockWare();
            Long skuId = item.getSkuId();
            skuStockWare.setSkuId(skuId);
            List<Long> wareIds = this.baseMapper.stockWareList(skuId);
            skuStockWare.setWareIds(wareIds);
            skuStockWare.setNumber(item.getCount());
            return skuStockWare;
        }).collect(Collectors.toList());
        if (skuStockWareList != null && skuStockWareList.size() > 0) {
            return dealLockStock(skuStockWareList, wareOrderTaskEntity);
        }
        return false;
    }


    private Boolean dealLockStock(List<SkuStockWare> skuStockWareList, WareOrderTaskEntity entity) {
        /**又返回结果，那么就开始进行锁定库存*/
        for (SkuStockWare skuStockWare : skuStockWareList) {
            boolean wareStockResult = false;
            /**获取库存列表*/
            Long skuId = skuStockWare.getSkuId();
            List<Long> wareIds = skuStockWare.getWareIds();
            if (wareIds == null) {
                throw new NoStockException(skuId);
            }
            for (Long wareId : wareIds) {
                /**返回如果是1 就代表锁定成功 反之则失败*/
                Long count = baseMapper.lockSkuStock(skuId, wareId, skuStockWare.getNumber());
                //1、如果每一个商品都锁定成功，将当前保存的商品信息发送给mq
                //2、锁定失败。前面的工作单信息就回滚了。发送出去的消息。即使要解锁记录，去数据库中查不到id。但是
                //   （1） 实际情况考虑，这种是不合理的。
                if (count == 1) {
                    wareStockResult = true;
                    //告诉rabbitmq，库存已经锁定，需要激活自动释放库存逻辑
                    WareOrderTaskDetailEntity wareOrderTaskDetailEntity = new WareOrderTaskDetailEntity(
                            null, skuId, "", skuStockWare.getNumber(), entity.getId(), wareId, 1
                    );
                    wareOrderTaskDetailService.save(wareOrderTaskDetailEntity);

                    StockLockTo stockLockTo = new StockLockTo();
                    StockDetailTo stockDetailTo = new StockDetailTo();
                    BeanUtils.copyProperties(wareOrderTaskDetailEntity, stockDetailTo);
                    stockLockTo.setWareTaskId(entity.getId());
                    stockLockTo.setStockDetailTo(stockDetailTo);
                    rabbitTemplate.convertAndSend("stock-event-exchange", "stock.locked", stockLockTo);
                    logger.info("给mq发送消息成功。stockLockTo: {}", stockLockTo);
                    break;
                } else {
                    /**如果当前锁定失败，就尝试下一个仓库*/
                }
            }
            if (!wareStockResult) {
                throw new NoStockException(skuId);
            }
        }
        return true;
    }

    @Data
    class SkuStockWare {
        private Long skuId;
        private List<Long> wareIds;
        private Integer number;
    }

}
