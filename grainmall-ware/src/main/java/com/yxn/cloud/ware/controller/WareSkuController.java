package com.yxn.cloud.ware.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.cloud.common.exception.BisCodeEnum;
import com.cloud.common.utils.R;
import com.yxn.vo.OrderResultVo;
import com.yxn.vo.SkuHasStockVo;
import com.yxn.vo.WareSkuLockVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.yxn.cloud.ware.entity.WareSkuEntity;
import com.yxn.cloud.ware.service.WareSkuService;
import com.yxn.common.utils.PageUtils;



/**
 * 商品库存
 *
 * @author lisw
 * @email lisw@gmail.com
 * @date 2020-11-25 17:21:03
 */
@RestController
@RequestMapping("ware/waresku")
public class WareSkuController {
    @Autowired
    private WareSkuService wareSkuService;

    @PostMapping("/lock/order")
    public R orderLockStock(@RequestBody WareSkuLockVo vo){
        /**锁库存*/
        Boolean result = null;
        try {
            result = wareSkuService.orderLockStock(vo);
            return R.ok();
        } catch (Exception e) {
            e.printStackTrace();
            return R.error(BisCodeEnum.NO_STOCK_EXCEPTION.getCode()
                    , BisCodeEnum.NO_STOCK_EXCEPTION.getMsg());
        }
    }

    /**查询skus是否还有库存*/
    @PostMapping("/hasstock")
    public List<SkuHasStockVo> getSkusHasStock(@RequestBody List<Long> skuIds){
        List<SkuHasStockVo> vos =  wareSkuService.getSkuHasStock(skuIds);
//        R<List<SkuHasStockVo>> ok = R.ok();
//        ok.setData(vos);
        return vos;
    }

    /**
     * 列表
     */
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = wareSkuService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    public R info(@PathVariable("id") Long id){
		WareSkuEntity wareSku = wareSkuService.getById(id);

        return R.ok().put("wareSku", wareSku);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody WareSkuEntity wareSku){
		wareSkuService.save(wareSku);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody WareSkuEntity wareSku){
		wareSkuService.updateById(wareSku);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Long[] ids){
		wareSkuService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
