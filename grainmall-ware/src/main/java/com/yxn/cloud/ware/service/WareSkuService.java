package com.yxn.cloud.ware.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yxn.vo.OrderResultVo;
import com.yxn.vo.SkuHasStockVo;
import com.yxn.common.utils.PageUtils;
import com.yxn.cloud.ware.entity.WareSkuEntity;
import com.yxn.vo.WareSkuLockVo;

import java.util.List;
import java.util.Map;

/**
 * 商品库存
 *
 * @author lisw
 * @email lisw@gmail.com
 * @date 2020-11-25 17:21:03
 */
public interface WareSkuService extends IService<WareSkuEntity> {

    PageUtils queryPage(Map<String, Object> params);

    List<SkuHasStockVo> getSkuHasStock(List<Long> skuIds);

    Boolean orderLockStock(WareSkuLockVo vo);
}

