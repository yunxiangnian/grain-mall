package com.yxn.cloud.ware.dao;

import com.yxn.cloud.ware.entity.PurchaseEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 采购信息
 * 
 * @author lisw
 * @email lisw@gmail.com
 * @date 2020-11-25 17:21:03
 */
@Mapper
public interface PurchaseDao extends BaseMapper<PurchaseEntity> {
	
}
