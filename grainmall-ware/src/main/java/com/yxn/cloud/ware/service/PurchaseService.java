package com.yxn.cloud.ware.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yxn.vo.MergeVo;
import com.yxn.common.utils.PageUtils;
import com.yxn.cloud.ware.entity.PurchaseEntity;

import java.util.Map;

/**
 * 采购信息
 *
 * @author lisw
 * @email lisw@gmail.com
 * @date 2020-11-25 17:21:03
 */
public interface PurchaseService extends IService<PurchaseEntity> {

    PageUtils queryPage(Map<String, Object> params);

    PageUtils queryPageUnreceivePurchase(Map<String, Object> params);

    void mergePurchase(MergeVo mergeVo);

}

