package com.yxn.cloud.ware.exception;

/**
 * @author lisw
 * @create 2021/8/2 21:03
 */
public class NoStockException extends RuntimeException{
    private Long skuId;
    public NoStockException(Long skuId){
        super("商品id是：" + skuId + "的，没有了库存");
    }

    public Long getSkuId() {
        return skuId;
    }

    public void setSkuId(Long skuId) {
        this.skuId = skuId;
    }
}
