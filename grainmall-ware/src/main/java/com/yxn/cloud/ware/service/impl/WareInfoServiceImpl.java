package com.yxn.cloud.ware.service.impl;

import com.alibaba.fastjson.TypeReference;
import com.cloud.common.utils.R;
import com.yxn.feign.MemberFeignService;
import com.yxn.vo.MemberAddressVo;
import org.apache.commons.lang.StringUtils;
import org.springframework.cloud.commons.util.IdUtils;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Map;
import java.util.Random;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yxn.common.utils.PageUtils;
import com.yxn.common.utils.Query;

import com.yxn.cloud.ware.dao.WareInfoDao;
import com.yxn.cloud.ware.entity.WareInfoEntity;
import com.yxn.cloud.ware.service.WareInfoService;

import javax.annotation.Resource;


@Service("wareInfoService")
public class WareInfoServiceImpl extends ServiceImpl<WareInfoDao, WareInfoEntity> implements WareInfoService {

    @Resource
    MemberFeignService memberFeignService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        QueryWrapper<WareInfoEntity> wareInfoEntityQueryWrapper = new QueryWrapper<>();
        String key = (String) params.get("key");
        if(!StringUtils.isEmpty(key)){
            wareInfoEntityQueryWrapper.eq("id",key).or()
                    .like("name",key)
                    .or().like("address",key)
                    .or().like("areacode",key);
        }

        IPage<WareInfoEntity> page = this.page(
                new Query<WareInfoEntity>().getPage(params),
                wareInfoEntityQueryWrapper
        );

        return new PageUtils(page);
    }

    @Override
    public BigDecimal getFare(Long addrId) {
        R addrinfoR = memberFeignService.addrInfo(addrId);
        MemberAddressVo addressVo  = addrinfoR.getData(new TypeReference<MemberAddressVo>() {
        });
        if (addressVo != null){
            //todo 需要调用物流服务的第三方接口去计算运费
            return new BigDecimal(addressVo.getPhone().substring(0, 3));
        }
        return null;
    }

}
