package com.yxn.feign;

import com.cloud.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author lisw
 * @create 2021/8/15 15:19
 */
@FeignClient(name = "grainmall-order-service")
public interface OrderFeignService {

    @GetMapping("/order/order/status/{orderSn}")
    R getOrderStatusByOrderSn(@PathVariable String orderSn);
}
