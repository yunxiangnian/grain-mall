package com.cloud.grainmall.order;

import com.yxn.GrainmallOrderApplication;
import com.yxn.cloud.order.entity.OrderReturnReasonEntity;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.Date;

@Slf4j
@SpringBootTest(classes = GrainmallOrderApplication.class)
@RunWith(SpringRunner.class)
class GrainmallOrderApplicationTests {

    @Resource
    AmqpAdmin amqpAdmin;

    @Resource
    RabbitTemplate rabbitTemplate;

    @Resource
    AmqpTemplate amqpTemplate;

    @Test
    void sendMessage(){
        OrderReturnReasonEntity reasonEntity = new OrderReturnReasonEntity();
        reasonEntity.setCreateTime(new Date());
        reasonEntity.setId(1L);
        reasonEntity.setName("哈哈哈");
        //如果发送的是一个对象，必须要求对象实现Serializable接口
        rabbitTemplate.convertAndSend("CloudMissing-exchange","RK",reasonEntity);
        log.info("消息发送成功，内容是{}",reasonEntity);
    }

    @Test
    void declareExchange() {
        DirectExchange directExchange = new DirectExchange("CloudMissing-exchange", true, false);
        amqpAdmin.declareExchange(directExchange);
        log.info("Exchange声明成功...");
    }



    @Test
    void declareQueue(){
        Queue queue = new Queue("CloudMissing-queue");
        amqpAdmin.declareQueue(queue);
        log.info("队列创建成功");
    }

    @Test
    void binding(){
        /**
         * String destination, 目的地
         * DestinationType destinationType, 目的地类型
         * String exchange, 交换机
         * String routingKey, routingKey
         * @Nullable Map<String, Object> arguments 参数
         * 将exchange指定的交换机和destination进行绑定，并指明目的地的类型，使用routingKey作为key
         */
        Binding binding = new Binding("CloudMissing-queue",
                Binding.DestinationType.QUEUE,"CloudMissing-exchange",
                "RK",null);
        amqpAdmin.declareBinding(binding);
        log.info("绑定创建成功");
    }
}
