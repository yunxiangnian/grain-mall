package com.yxn.cloud.order.vo;

import com.yxn.cloud.order.entity.OrderEntity;
import lombok.Data;

/**
 * @author lisw
 * @create 2021/8/1 15:36
 */
@Data
public class OrderSubmitRespVo {
    /**订单的实体类*/
    private OrderEntity orderEntity;
    /**0是成功 非0为各种错误的类型*/
    private Integer code;

}
