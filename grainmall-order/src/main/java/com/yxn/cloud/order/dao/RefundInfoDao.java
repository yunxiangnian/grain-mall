package com.yxn.cloud.order.dao;

import com.yxn.cloud.order.entity.RefundInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 退款信息
 * 
 * @author lisw
 * @email lisw@gmail.com
 * @date 2020-11-25 15:41:53
 */
@Mapper
public interface RefundInfoDao extends BaseMapper<RefundInfoEntity> {
	
}
