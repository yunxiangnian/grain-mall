package com.yxn.cloud.order.to;

import com.yxn.cloud.order.entity.OrderEntity;
import com.yxn.cloud.order.entity.OrderItemEntity;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author lisw
 * @create 2021/8/1 16:34
 */
@Data
public class OrderCreateTo {
    private OrderEntity entity;

    private List<OrderItemEntity> items;

    /**计算应付的运费*/
    private BigDecimal fare;
    /**计算应付的价格*/
    private BigDecimal payPrice;
}
