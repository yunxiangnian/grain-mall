package com.yxn.cloud.order.vo;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.awt.font.NumericShaper;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * @author lisw
 * @create 2021/7/26 15:46
 * 订单确认页所用到的所有信息
 */

public class OrderConfirmVo {

    /**收获地址列表*/
    @Getter@Setter
    List<MemberAddressVo> memberAddress;

    /**所有选中的购物项*/
    @Getter@Setter
    List<OrderItemVo> orderItems;

    @Getter@Setter
    Map<Long,Boolean> itemStockMap;

    /**发票记录*/

    /**优惠券信息...这里以积分为例*/
    @Getter@Setter
    Integer integration;

    /**订单的防重令牌*/
    @Getter@Setter
    String orderToken;

    /**订单总额*/
    BigDecimal orderTotalAmount;

    public BigDecimal getOrderTotalAmount() {
        BigDecimal orderTotalAmount = new BigDecimal("0");
        if (orderItems == null){
            return orderTotalAmount;
        }else{
            for (OrderItemVo orderItemVo : orderItems){
                BigDecimal orderItemVoPrice = orderItemVo.getPrice().
                        multiply(new BigDecimal(orderItemVo.getCount().toString()));
                orderTotalAmount = orderTotalAmount.add(orderItemVoPrice);
            }
            return orderTotalAmount;
        }

    }

    /**应付价格*/
    BigDecimal shouldPayPrice;

    public BigDecimal getShouldPayPrice() {
        return getOrderTotalAmount();
    }

    private Integer count;

    public Integer getCount() {
        int number = 0;
        if (orderItems == null){
            return number;
        }else{
            for (OrderItemVo orderItemVo : orderItems){
                number = number + orderItemVo.getCount();
            }
            return number;
        }
    }
}
