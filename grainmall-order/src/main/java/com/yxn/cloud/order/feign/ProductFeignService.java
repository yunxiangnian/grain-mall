package com.yxn.cloud.order.feign;

import com.cloud.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author lisw
 * @create 2021/8/1 17:36
 */
@FeignClient("grainmall-product-service")
public interface ProductFeignService {
    /**
     * 获取商品的spu信息
     * @param skuId
     * @return
     */
    @GetMapping("/product/spuinfo/skuId/{id}")
    R getSpuBySkuId(@PathVariable("id")Long skuId);
}
