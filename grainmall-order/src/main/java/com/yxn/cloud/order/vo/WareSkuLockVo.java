package com.yxn.cloud.order.vo;

import lombok.Data;

import java.util.List;

/**
 * @author lisw
 * @create 2021/8/1 22:00
 */
@Data
public class WareSkuLockVo {
    /**订单号*/
    private String orderSn;

    /**锁定库存的数量*/
    private List<OrderItemVo> items;

}
