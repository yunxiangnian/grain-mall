package com.yxn.cloud.order.config;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

/**
 * @author lisw
 * @create 2021/7/17 22:19
 */
@Configuration
public class MyRabbitConfig {

    @Resource
    RabbitTemplate rabbitTemplate;

    /**
     * 对消息的转换使用Json方式序列化
     * @return
     */
    @Bean
    public MessageConverter messageConverter() {
        return new Jackson2JsonMessageConverter();
    }

    /**
     * 1、定制rabbitTemplate
     *
     */
    @PostConstruct
    public void initConfirmCallback(){
        //2、设置broker收到消息确认回调
        rabbitTemplate.setConfirmCallback(new RabbitTemplate.ConfirmCallback(){
            /**
             * 只要消息抵达broker，那么就会触发该方法
             * @param correlationData 当前消息的唯一关联数据（这个消息大部分情况下是消息的唯一ID）
             * @param ack 返回是否成功的结果
             * @param cause 如果失败了，那就是失败的原因
             */
            @Override
            public void confirm(CorrelationData correlationData, boolean ack, String cause) {
                System.out.println("关联信息：" + correlationData + "，是否成功：" + ack + "，原因：" + cause);
            }
        });
        //3、设置队列收到消息确认回调
        rabbitTemplate.setReturnCallback(new RabbitTemplate.ReturnCallback() {
            /**
             * 触发时机：只要消息没有投递给指定的队列，就会触发这个类似投递失败的回调方法
             * @param message 返回的消息
             * @param replyCode 返回的状态码
             * @param replyText 返回的文本
             * @param exchange 从那个交换机返回的
             * @param routingKey 对应的路由key
             */
            @Override
            public void returnedMessage(Message message, int replyCode,
                                        String replyText, String exchange, String routingKey) {
                System.out.println(message.getBody() + "返回码："+replyCode + "，返回文本:" + replyText+"," +
                        "当时发送给的交换机：" + exchange + ",对应的路由key：" + routingKey);
            }
        });
    }

}
