package com.yxn.cloud.order.dao;

import com.yxn.cloud.order.entity.PaymentInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 支付信息表
 * 
 * @author lisw
 * @email lisw@gmail.com
 * @date 2020-11-25 15:41:53
 */
@Mapper
public interface PaymentInfoDao extends BaseMapper<PaymentInfoEntity> {
	
}
