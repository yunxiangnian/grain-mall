package com.yxn.cloud.order.controller;

import com.cloud.common.utils.R;
import com.yxn.cloud.order.entity.OrderEntity;
import com.yxn.cloud.order.entity.OrderReturnReasonEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Date;
import java.util.UUID;

/**
 * @author lisw
 * @create 2021/7/18 11:03
 */
@RestController
@Slf4j
public class AMQPController {

    @Resource
    RabbitTemplate rabbitTemplate;

    @GetMapping("/sendMsg")
    public R sendMq(@RequestParam(value = "number",defaultValue = "10")Integer number){
        for (int i = 0; i < number; i++) {
            if (i%2 == 0){
                OrderReturnReasonEntity reasonEntity = new OrderReturnReasonEntity();
                reasonEntity.setCreateTime(new Date());
                reasonEntity.setId(1L);
                reasonEntity.setName("哈哈哈" + i);
                //如果发送的是一个对象，必须要求对象实现Serializable接口
                rabbitTemplate.convertAndSend("CloudMissing-exchange","RK",reasonEntity);
                log.info("消息发送成功，内容是{}",reasonEntity);
            }else {
                OrderEntity orderEntity = new OrderEntity();
                orderEntity.setOrderSn(UUID.randomUUID().toString());
                rabbitTemplate.convertAndSend("CloudMissing-exchange","RK",orderEntity);
                log.info("订单消息发送成功...");
            }
        }
       return R.ok();
    }

}
