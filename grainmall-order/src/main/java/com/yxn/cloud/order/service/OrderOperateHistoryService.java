package com.yxn.cloud.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yxn.common.utils.PageUtils;
import com.yxn.cloud.order.entity.OrderOperateHistoryEntity;

import java.util.Map;

/**
 * 订单操作历史记录
 *
 * @author lisw
 * @email lisw@gmail.com
 * @date 2020-11-25 15:41:53
 */
public interface OrderOperateHistoryService extends IService<OrderOperateHistoryEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

