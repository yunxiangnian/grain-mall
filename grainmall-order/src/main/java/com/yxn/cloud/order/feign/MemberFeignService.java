package com.yxn.cloud.order.feign;

import com.cloud.common.utils.R;
import com.yxn.cloud.order.vo.MemberAddressVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * @author lisw
 * @create 2021/7/26 16:03
 */
@FeignClient("grainmall-member-service")
public interface MemberFeignService {

    @GetMapping("/member/memberreceiveaddress/{memberId}/addresses")
    List<MemberAddressVo> queryMemberAddr(@PathVariable("memberId")Long memberId);

    /**
     * 远程查询地址，通过地址id的方法
     * @param addressId
     * @return
     */
    @RequestMapping("/member/memberreceiveaddress/info/{id}")
    R queryAddressById(@PathVariable("id") Long addressId);
}
