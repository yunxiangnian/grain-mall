package com.yxn.cloud.order.vo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author lisw
 * @create 2021/8/1 13:54
 * 订单提交页面的VO
 */
@Data
public class OrderSubmitVo {
    /**收获地址id*/
    private Long addrId;
    /**支付类型*/
    private String payType;
    /**无需上传用户提交的商品信息，再次从redis中查询*/

    /**页面的令牌*/
    private String orderToken;
    /**应付的价格，  可以实现再次跟数据库中选择的商品总价进行比对，实现验价的功能*/
    private BigDecimal payPrice;
    /**订单的备注*/
    private String note;
}
