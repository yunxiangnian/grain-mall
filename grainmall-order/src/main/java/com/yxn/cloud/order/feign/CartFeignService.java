package com.yxn.cloud.order.feign;

import com.yxn.cloud.order.vo.OrderItemVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

/**
 * @author lisw
 * @create 2021/7/26 16:31
 */
@FeignClient("grainmall-cart-service")
public interface CartFeignService {
    @GetMapping("/currentUserCartItems")
    List<OrderItemVo> getCurrentUserItems();
}
