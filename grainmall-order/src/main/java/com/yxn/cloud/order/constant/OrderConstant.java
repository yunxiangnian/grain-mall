package com.yxn.cloud.order.constant;

/**
 * @author lisw
 * @create 2021/8/1 13:27
 */
public class OrderConstant {
    public static final String USER_ORDER_TOKEN_PREFIX = "order:token:";
}
