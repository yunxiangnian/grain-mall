package com.yxn.cloud.order.web;

import com.yxn.cloud.order.config.AlipayConfig;
import com.yxn.cloud.order.service.OrderService;
import com.yxn.cloud.order.vo.PayVo;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.logging.log4j2.Log4j2AbstractLoggerImpl;
import org.slf4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

/**
 * @author lisw
 * @create 2021/8/18 22:00
 */
@Controller
@Slf4j
public class PayWebController {

    @Resource
    AlipayConfig alipayConfig;

    @Resource
    OrderService orderService;

    /**
     * produces = "text/html" 声明返回的数据类型是html
     * @param orderSn
     * @return
     * @throws Exception
     */
    @GetMapping(value = "/payOrder" , produces = "text/html")
    @ResponseBody
    public String payOrder(@RequestParam("orderSn") String orderSn) throws Exception {
        PayVo payVo = orderService.getOrderPay(orderSn);
        String result = alipayConfig.pay(payVo);
        log.info("返回的结果是: {}", result);
        return result;
    }
}
