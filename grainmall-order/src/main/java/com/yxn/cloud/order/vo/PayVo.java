package com.yxn.cloud.order.vo;

import lombok.Data;

/**
 * @author lisw
 * @create 2021/8/18 21:50
 */
@Data
public class PayVo {
    // 商品订单号 必填
    private String out_trade_no;
    // 订单名称  必填
    private String subject;
    // 付款金额 必填
    private String total_amount;
    // 商品描述 可空
    private String body;
}
