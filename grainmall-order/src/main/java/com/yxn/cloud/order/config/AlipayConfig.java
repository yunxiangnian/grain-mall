package com.yxn.cloud.order.config;

import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.domain.AlipayTradePrecreateModel;
import com.alipay.api.request.AlipayOpenPublicTemplateMessageIndustryModifyRequest;
import com.alipay.api.request.AlipayTradePrecreateRequest;
import com.alipay.api.response.AlipayOpenPublicTemplateMessageIndustryModifyResponse;
import com.alipay.api.response.AlipayTradePrecreateResponse;
import com.yxn.cloud.order.vo.PayVo;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author lisw
 * @create 2021/8/18 21:34
 */
@Component
@ConfigurationProperties(prefix = "alipay")
@Data
public class AlipayConfig {

    private String APP_ID;
    private String APP_PRIVATE_KEY;
    private String ALIPAY_PUBLIC_KEY;
    private String CHARSET;
    private String RETURN_URL;
    private String NOTIFY_URL;


    public String pay(PayVo vo) throws Exception {
        //实例化客户端
        AlipayClient alipayClient = new DefaultAlipayClient("https://openapi.alipay.com/gateway.do", APP_ID, APP_PRIVATE_KEY, "json", CHARSET, ALIPAY_PUBLIC_KEY, "RSA2");
        //实例化具体API对应的request类,类名称和接口名称对应,当前调用接口名称：alipay.open.public.template.message.industry.modify
        AlipayOpenPublicTemplateMessageIndustryModifyRequest request = new AlipayOpenPublicTemplateMessageIndustryModifyRequest();
        //SDK已经封装掉了公共参数，这里只需要传入业务参数
        //此次只是参数展示，未进行字符串转义，实际情况下请转义
        String out_trade_no = vo.getOut_trade_no();
        String subject = vo.getSubject();
        String total_amount = vo.getTotal_amount();
        String body = vo.getBody();
        request.setBizContent("  {" +
                "    \"out_trade_no\":\"" + out_trade_no + "\"," +
                "    \"total_amount\":\"" + total_amount + "\"," +
                "    \"subject\":\"" + subject + "\"," +
                "    \"body\":\"" + body + "\"," +
                " }");
        request.setReturnUrl(RETURN_URL);
        request.setNotifyUrl(NOTIFY_URL);
        AlipayOpenPublicTemplateMessageIndustryModifyResponse response = alipayClient.execute(request);
        //调用成功，则处理业务逻辑
        if (response.isSuccess()) {
            //.....
        }
        return response.getBody();
    }
}
