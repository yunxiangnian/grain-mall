package com.yxn.cloud.order.enums;

/**
 * @author lisw
 * @create 2021/8/1 21:26
 */
public enum OrderStatusEnum {
    /**待付款*/
    CREATE_NEW(0,"待付款"),
    /**已付款*/
    PAYED(1,"已付款"),
    /**已发货*/
    SENDED(2,"已发货"),
    /**已收货*/
    RECEIVED(3,"已收货"),
    /**已取消*/
    CANCELED(4,"已取消"),
    /**售后服务中*/
    SERVICING(5,"售后服务中"),
    /**服务完成*/
    SERVICED(6,"服务完成");


    private Integer code;
    private String message;

    OrderStatusEnum(Integer code,String message){
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
