package com.yxn.cloud.order.vo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author lisw
 * @create 2021/7/26 15:54
 */
@Data
public class OrderItemVo {
    private Long skuId;
    private String title;
    private String image;
    /**sku的销售属性*/
    private List<String> skuAttr;
    private BigDecimal price;
    private Integer count;
    private BigDecimal totalPrice;
    private BigDecimal itemWeight;
}
