package com.yxn.cloud.order.web;

import com.yxn.cloud.order.exception.NoStockException;
import com.yxn.cloud.order.service.OrderService;
import com.yxn.cloud.order.vo.OrderConfirmVo;
import com.yxn.cloud.order.vo.OrderSubmitRespVo;
import com.yxn.cloud.order.vo.OrderSubmitVo;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.annotation.Resource;

/**
 * @author lisw
 * @create 2021/7/26 15:29
 */
@Controller
public class OrderWebController {

    @Resource
    OrderService orderService;

    @GetMapping("/toTrade")
    public String toTrade(Model model){
        OrderConfirmVo orderConfirmVo = orderService.confirmOrder();

        model.addAttribute("orderConfirm", orderConfirmVo);
        return "confirm";
    }

    /**
     * 下单
     * 所有订单要提交的数据
     * @param vo
     * @return
     */
    @PostMapping("/submitOrder")
    public String submitOrder(OrderSubmitVo vo, Model model,
                              RedirectAttributes redirectAttributes){
       try {
           OrderSubmitRespVo respVo = orderService.submitOrder(vo);
           if (respVo.getCode() == 0){
               //下单成功来到支付选择页
               model.addAttribute("submitOrderResp",respVo);
               return "pay";
           }else {
               String msg = "";
               switch (respVo.getCode()) {
                   case 1: msg += "令牌校验失败。订单信息过期，请刷新再次提交"; break;
                   case 2: msg += "订单商品价格发生变化，请确认后再次提交"; break;
                   case 3: msg += "库存锁定失败，商品库存不足。";break;
                   default: msg += "";
               }
               redirectAttributes.addFlashAttribute("msg" , msg );
               return "redirect:http://order.grainmall.com:81/toTrade";
           }
       }catch (Exception e){
           if (e instanceof NoStockException) {
               String message = ((NoStockException) e).getMessage();
               redirectAttributes.addFlashAttribute("msg" , message );
           }
           return "redirect:http://order.grainmall.com:81/toTrade";
       }
    }
}
