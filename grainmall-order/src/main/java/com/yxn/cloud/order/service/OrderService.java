package com.yxn.cloud.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yxn.cloud.order.vo.OrderConfirmVo;
import com.yxn.cloud.order.vo.OrderSubmitRespVo;
import com.yxn.cloud.order.vo.OrderSubmitVo;
import com.yxn.cloud.order.vo.PayVo;
import com.yxn.common.utils.PageUtils;
import com.yxn.cloud.order.entity.OrderEntity;

import java.util.Map;

/**
 * 订单
 *
 * @author lisw
 * @email lisw@gmail.com
 * @date 2020-11-25 15:41:53
 */
public interface OrderService extends IService<OrderEntity> {

    PageUtils queryPage(Map<String, Object> params);

    OrderConfirmVo confirmOrder();

    OrderSubmitRespVo submitOrder(OrderSubmitVo vo);

    OrderEntity getOrderByOrderSn(String orderSn);

    void closeOrder(OrderEntity orderEntity);

    PayVo getOrderPay(String orderSn);
}

