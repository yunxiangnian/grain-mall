package com.cloud.grainmall.controller;

import com.cloud.common.utils.R;
import com.cloud.grainmall.component.SmsComponent;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author lisw
 * @create 2021/7/13 21:32
 */
@RestController
@RequestMapping("/sms")
public class SmsSendController {
    @Resource
    private SmsComponent component;

    @GetMapping("/sendCode")
    public R sendSms(@RequestParam("phone")String phone,@RequestParam("code")String code){
        component.sendSms(phone, code);
        return R.ok();
    }

}
