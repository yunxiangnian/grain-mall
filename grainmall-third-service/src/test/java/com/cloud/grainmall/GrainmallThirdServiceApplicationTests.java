package com.cloud.grainmall;

import com.aliyun.oss.OSSClient;
import com.cloud.grainmall.component.SmsComponent;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.io.ByteArrayInputStream;

@SpringBootTest
class GrainmallThirdServiceApplicationTests {

    @Resource
    OSSClient ossClient;

    @Value("${spring.cloud.alicloud.access-key}")
    private String accessKey;

    @Resource
    SmsComponent smsComponent;

    @Test
    void contextLoads() {
        System.out.println(accessKey);
    }


    @Test
    void sendSms() {
        smsComponent.sendSms("17630838209", "114042");
    }

    @Test
    public void testUploadToAliyun() {
        // Endpoint以杭州为例，其它Region请按实际情况填写。
        String bucketName = "grain-hello";
        String objectName = "test-String";

        String content = "First OSS Test With Third-Service and Config";
        ossClient.putObject(bucketName, objectName, new ByteArrayInputStream(content.getBytes()));
        // 关闭OSSClient。
        ossClient.shutdown();
        System.out.println("上传成功");
    }
}
