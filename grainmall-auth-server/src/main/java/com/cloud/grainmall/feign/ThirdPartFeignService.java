package com.cloud.grainmall.feign;

import com.cloud.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author lisw
 * @create 2021/7/13 21:38
 */
@FeignClient("grain-third-service")
public interface ThirdPartFeignService {
    @GetMapping("/sms/sendCode")
    public R sendSms(@RequestParam("phone")String phone, @RequestParam("code")String code);
}
