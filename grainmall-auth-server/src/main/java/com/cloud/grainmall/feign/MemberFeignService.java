package com.cloud.grainmall.feign;

import com.cloud.common.utils.R;
import com.cloud.common.vo.UserLoginVo;
import com.cloud.common.vo.UserRegistVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @author lisw
 * @create 2021/7/14 13:04
 */
@FeignClient("grainmall-member-service")
public interface MemberFeignService {
    @PostMapping("/member/member/regist")
    R regist(@RequestBody UserRegistVo vo);

    @PostMapping("/member/member/login")
    R login(@RequestBody UserLoginVo vo);
}
