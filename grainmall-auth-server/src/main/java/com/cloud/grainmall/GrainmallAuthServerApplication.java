package com.cloud.grainmall;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * @author 11692
 * EnableRedisHttpSession //整合redis作为session存储
 */
@EnableRedisHttpSession
@SpringBootApplication
@EnableFeignClients
@EnableDiscoveryClient
public class GrainmallAuthServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(GrainmallAuthServerApplication.class, args);
    }

}
