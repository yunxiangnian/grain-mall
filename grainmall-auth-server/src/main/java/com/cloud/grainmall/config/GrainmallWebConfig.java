package com.cloud.grainmall.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author lisw
 * @create 2021/7/13 14:39
 */
@Configuration
@EnableWebMvc
public class GrainmallWebConfig implements WebMvcConfigurer {

    /**
     * 使用这个配置类，来替代写空的web方法去跳转页面
     * 视图映射
     * @param registry
     */
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("register").setViewName("register");
    }
}
