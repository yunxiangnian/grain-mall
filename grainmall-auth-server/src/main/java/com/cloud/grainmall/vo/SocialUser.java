package com.cloud.grainmall.vo;

import lombok.Data;

/**
 * @author lisw
 * @create 2021/7/25 16:26
 */
@Data
public class SocialUser {
    private String access_token;
    private String remind_in;
    private long expires_in;
    private String uid;
    private String isRealName;
}
