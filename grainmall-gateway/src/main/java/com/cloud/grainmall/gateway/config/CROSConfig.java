package com.cloud.grainmall.gateway.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.reactive.CorsWebFilter;
import org.springframework.web.cors.reactive.UrlBasedCorsConfigurationSource;

/**
 * @author lisw
 * @create 2021/6/27 13:35
 * 跨域配置类
 */
@Configuration
public class CROSConfig {
    /**
     * 配置跨域过滤器
     * @return
     */
    @Bean
    public CorsWebFilter corsWebFilter(){
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();

        CorsConfiguration configuration = new CorsConfiguration();
        /**配置跨域请求*/
        //添加所有的请求头
        configuration.addAllowedHeader("*");
        //允许所有的方法都可访问
        configuration.addAllowedMethod("*");
        //允许请求来源为所有
        configuration.addAllowedOrigin("*");
        /**允许带cookie请求*/
        configuration.setAllowCredentials(true);

        source.registerCorsConfiguration("/**",configuration);
        return new CorsWebFilter(source);
    }
}
