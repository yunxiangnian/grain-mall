package com.cloud.grainmall.coupon;

import com.yxn.GrainmallCouponApplication;
import com.yxn.cloud.coupon.entity.CouponEntity;
import com.yxn.cloud.coupon.service.CouponService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = GrainmallCouponApplication.class)
class GrainmallCouponApplicationTests {

    @Resource
    CouponService couponService;

    @Test
    void contextLoads() {
        CouponEntity couponEntity = new CouponEntity();
        couponEntity.setCouponName("天天满减");
        couponEntity.setNote("不能多次使用");
        couponService.save(couponEntity);
        System.out.println("保存成功");
    }

}
