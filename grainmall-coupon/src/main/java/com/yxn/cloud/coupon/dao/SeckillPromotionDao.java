package com.yxn.cloud.coupon.dao;

import com.yxn.cloud.coupon.entity.SeckillPromotionEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 秒杀活动
 * 
 * @author lisw
 * @email lisw@gmail.com
 * @date 2020-11-25 15:33:17
 */
@Mapper
public interface SeckillPromotionDao extends BaseMapper<SeckillPromotionEntity> {
	
}
