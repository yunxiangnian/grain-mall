package com.yxn.cloud.coupon.dao;

import com.yxn.cloud.coupon.entity.SkuLadderEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品阶梯价格
 * 
 * @author lisw
 * @email lisw@gmail.com
 * @date 2020-11-25 15:33:18
 */
@Mapper
public interface SkuLadderDao extends BaseMapper<SkuLadderEntity> {
	
}
