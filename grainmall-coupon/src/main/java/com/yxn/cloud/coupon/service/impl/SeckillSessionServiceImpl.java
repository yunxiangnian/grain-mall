package com.yxn.cloud.coupon.service.impl;

import com.yxn.cloud.coupon.entity.SeckillSkuRelationEntity;
import com.yxn.cloud.coupon.service.SeckillSkuRelationService;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yxn.common.utils.PageUtils;
import com.yxn.common.utils.Query;

import com.yxn.cloud.coupon.dao.SeckillSessionDao;
import com.yxn.cloud.coupon.entity.SeckillSessionEntity;
import com.yxn.cloud.coupon.service.SeckillSessionService;

import javax.annotation.Resource;


@Service("seckillSessionService")
public class SeckillSessionServiceImpl extends ServiceImpl<SeckillSessionDao, SeckillSessionEntity> implements SeckillSessionService {

    @Resource
    private SeckillSkuRelationService relationService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<SeckillSessionEntity> page = this.page(
                new Query<SeckillSessionEntity>().getPage(params),
                new QueryWrapper<SeckillSessionEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public List<SeckillSessionEntity> getLatest3DaySession() {
        // 计算三天内的时间  使用Java8新特性  LocalDate(Time)类
        // LocalDate 只包含日期 LocalTime 只包含时间
        // LocalDateTime 包含日期和时间
        List<SeckillSessionEntity> sessionList = this.list(new QueryWrapper<SeckillSessionEntity>().between(
                "start_time", buildStartTime(), buildEndTime()));
        if (sessionList != null && sessionList.size() > 0) {
            // 处理session关联的sku数据
            List<SeckillSessionEntity> sessionEntities = sessionList.stream().map(item -> {
                Long id = item.getId();
                List<SeckillSkuRelationEntity> session_relations_sku = relationService.list(new QueryWrapper<SeckillSkuRelationEntity>()
                        .eq("promotion_session_id", id));
                item.setRelationSkus(session_relations_sku);
                return item;
            }).collect(Collectors.toList());

            return sessionEntities;
        }

        return null;
    }

    private String buildStartTime() {
        LocalDate now = LocalDate.now();
        // 00.00.00
        LocalTime min = LocalTime.MIN;
        // 2021-08-22 00.00.00
        LocalDateTime startTime = LocalDateTime.of(now, min);
        String start = startTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        return start;
    }

    private String buildEndTime() {
        LocalDate towDayLater = LocalDate.now().plusDays(2);
        // 00.00.00
        LocalTime max = LocalTime.MAX;
        // 2021-08-24 23.59.59
        LocalDateTime endTime = LocalDateTime.of(towDayLater, max);
        String end = endTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        return end;
    }

}
