package com.yxn.cloud.coupon.service.impl;

import com.cloud.common.to.MemberPrice;
import com.cloud.common.to.SkuReductionTo;
import com.yxn.cloud.coupon.entity.MemberPriceEntity;
import com.yxn.cloud.coupon.entity.SkuLadderEntity;
import com.yxn.cloud.coupon.service.MemberPriceService;
import com.yxn.cloud.coupon.service.SkuLadderService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yxn.common.utils.PageUtils;
import com.yxn.common.utils.Query;

import com.yxn.cloud.coupon.dao.SkuFullReductionDao;
import com.yxn.cloud.coupon.entity.SkuFullReductionEntity;
import com.yxn.cloud.coupon.service.SkuFullReductionService;

import javax.annotation.Resource;


@Service("skuFullReductionService")
public class SkuFullReductionServiceImpl extends ServiceImpl<SkuFullReductionDao, SkuFullReductionEntity> implements SkuFullReductionService {

    @Resource
    SkuLadderService skuLadderService;

    @Resource
    MemberPriceService memberPriceService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<SkuFullReductionEntity> page = this.page(
                new Query<SkuFullReductionEntity>().getPage(params),
                new QueryWrapper<SkuFullReductionEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public void saveSkuReduction(SkuReductionTo reductionTo) {
        /**
         * 赋值满减等信息
         * */
        SkuLadderEntity skuLadderEntity = new SkuLadderEntity();
        skuLadderEntity.setId(reductionTo.getSkuId());
        skuLadderEntity.setDiscount(reductionTo.getDiscount());
        skuLadderEntity.setFullCount(reductionTo.getFullCount());
//        skuLadderEntity.setPrice(reductionTo.getReducePrice()); 有可能存在自动计算
        skuLadderService.save(skuLadderEntity);

        //2、sms_sku_full_reduction
        SkuFullReductionEntity reductionEntity = new SkuFullReductionEntity();
        BeanUtils.copyProperties(reductionTo,reductionEntity);
        if(reductionEntity.getFullPrice().compareTo(new BigDecimal("0"))==1){
            this.save(reductionEntity);
        }


        //3、sms_member_price
        List<MemberPrice> memberPrice = reductionTo.getMemberPrice();

        List<MemberPriceEntity> collect = memberPrice.stream().map(item -> {
            MemberPriceEntity priceEntity = new MemberPriceEntity();
            priceEntity.setSkuId(reductionTo.getSkuId());
            priceEntity.setMemberLevelId(item.getId());
            priceEntity.setMemberLevelName(item.getName());
            priceEntity.setMemberPrice(item.getPrice());
            priceEntity.setAddOther(1);
            return priceEntity;
        }).filter(item->{
            return item.getMemberPrice().compareTo(new BigDecimal("0")) == 1;
        }).collect(Collectors.toList());

        memberPriceService.saveBatch(collect);
    }

}
