package com.yxn.cloud.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yxn.common.utils.PageUtils;
import com.yxn.cloud.coupon.entity.SeckillSkuNoticeEntity;

import java.util.Map;

/**
 * 秒杀商品通知订阅
 *
 * @author lisw
 * @email lisw@gmail.com
 * @date 2020-11-25 15:33:18
 */
public interface SeckillSkuNoticeService extends IService<SeckillSkuNoticeEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

